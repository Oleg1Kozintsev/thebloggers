var instagramId = 0;
var proxy_settings = $("#proxy-settings");

$(function () {
    $('[data-toggle="popover"]').popover()
});

$("#enabled-proxy").on("change", function () {
    proxy_settings.toggle();

    if ($("#enabled-proxy").prop("checked")) {
        proxy_settings.find("input").removeAttr("disabled");
    } else {
        proxy_settings.find("input").attr("disabled", "disabled");
        proxy_settings.find(".form-group").removeClass("has-error");
    }
});

$("#add-account").on("click", function() {
    $("#account-form").trigger("reset");
    proxy_settings.find("input").attr("disabled", "disabled");
    proxy_settings.hide();
    $("#account-modal").modal("show");
    $("#btn-add-inst").click(function () {
        // todo: необходимо проверить заполненность атрибутов
        var login = $("#instagram_login").val();
        if (login.length < 1)
            return;
        var fd = new FormData();
        fd.append("login", login);
        fd.append("password", $("#instagram_password").val());
        fd.append("proxy_ip", $("#proxy_ip").val());
        fd.append("proxy_port", $("#proxy_port").val());
        fd.append("proxy_login", $("#proxy_login").val());
        fd.append("proxy_password", $("#proxy_password").val());

        var xhr = new XMLHttpRequest();
        xhr.upload.onload = function() {
            // действие после загрузки.
            // скрыть окно, добавить пункт в меню
            $("#account-modal").modal("hide");
        };

        xhr.upload.onerror = function() {
            alert( 'Произошла ошибка при загрузке данных на сервер!' );
        };

        xhr.open("post", "add-inst", true);
        xhr.send(fd);
    })
});

$(".user-panel").on("click", function() {
    instagramId = $(this).attr("id");
    console.log("Id: " + instagramId);
    $(".user-panel").removeClass("user-active");
    $("#" + instagramId).addClass("user-active");
    //
});
