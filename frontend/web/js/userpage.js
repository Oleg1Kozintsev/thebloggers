$('.first-top-foto').cropbox({
    width: 380,
    height: 380,
    showControls: 'never'
}, function() {
    //on load
    console.log('Url: ' + this.getDataURL());
}).on('cropbox', function(e, data) {
    console.log('crop window: ' + data);
});

$('.top-foto').cropbox({
    width: 175,
    height: 175,
    showControls: 'never'
}, function() {
    //on load
    console.log('Url: ' + this.getDataURL());
}).on('cropbox', function(e, data) {
    console.log('crop window: ' + data);
});

$('#location-form').on('beforeSubmit', function(){
    const data = $(this).serialize();
    const _url = $(this).attr('action');
    $.ajax({
        url: _url,
        type: 'POST',
        data: data,
        success: function(res){
            document.getElementById("save-info").hidden = false;
            setTimeout(function(){document.getElementById("save-info").hidden = true;}, 10000);
            console.log(res);
        },
        error: function(){
            console.log('Error!');
        }
    });
    return false;
});

$(document).on('click', '.showModalPost', function(){
    const postModal = $('#post-modal');
    if (!postModal.data('bs.modal').isShown) {
        //if modal isn't open; open it and load content
        postModal.modal('show');
        const textContent = $(this).attr('data-content');
        // заменяем
        const highlighted = textContent.replace(/(@\S+)/g, '<span class="hashtag">$1</span>').replace(/(#\S+)/g, '<span class="hashtag">$1</span>');

        document.getElementById('modalContent').innerHTML = '<img src="' + $(this).attr('data-url') + '" width="100%">' +
            '<p style="margin-top: 10px;">' + highlighted + '</p>';

        document.getElementById('img-like').innerHTML = $(this).attr('data-like');
        document.getElementById('img-comment').innerHTML = $(this).attr('data-comm');
        $(".post-url").attr("href", "https://www.instagram.com/p/" + $(this).attr('data-code') + "/");

    }
});

google.charts.load('current', {'packages': ['corechart', 'bar'], 'language': 'ru-Ru'});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    const id = findGetParameter('id');
    const x = new XMLHttpRequest();
    x.open("GET", "chart?id=" + id, true);
    x.onload = function () {
        const response = JSON.parse(x.responseText);
        const arrayFollowers = [
            ['Дата', 'Подписчиков'],
        ];
        const arrayMedia = [
            ['Дата', 'Записей'],
        ];
        const arrayER = [
            ['Дата', 'Значение']
        ];
        const arrayFollowing = [
            ['Дата', 'Подписок']
        ];
        let minFollowers = 0;
        let minMedia = 0;
        let minER = 0;
        let minFollowing = 0;
        response.forEach(function (item) {
            const dt = new Date(item['created_at'] * 1000);
            arrayFollowers.push([dt, item['followers_count']]);
            arrayMedia.push([dt, item['media_count']]);
            arrayER.push([dt, item['er']]);
            arrayFollowing.push([dt, item['followings_count']]);
            if (minFollowers === 0 && minMedia === 0) {
                minFollowers = item['followers_count'];
                minMedia = item['media_count'];
            }
            if (minFollowers > item['followers_count']) {
                minFollowers = item['followers_count'];
            }
            if (minMedia > item['media_count']) {
                minMedia = item['media_count'];
            }
            if (minER > item['er']){
                minER = item['er']
            }
            if (minFollowing > item['followings_count']){
                minFollowing = item['followings_count']
            }
        });
        let data = google.visualization.arrayToDataTable(arrayFollowers);

        const options = function (min) {
            return {
                legend: {position: 'none'},
                hAxis: {
                    title: '', titleTextStyle: {color: '#333'},
                    chartArea: {
                        left: 10,
                        right: 10, // !!! works !!!
                        bottom: 10,  // !!! works !!!
                        top: 10,
                        width: "100%",
                        height: "100%"
                    },
                    gridlines: {
                        count: -1,
                        units: {
                            days: {format: ['dd MMM']},
                            hours: {format: ['HH:mm', 'HH']},
                        }
                    },
                    minorGridlines: {
                        units: {
                            hours: {format: ['HH:mm', 'HH']},
                            minutes: {format: ['HH:mm', ':mm']}
                        }
                    }
                },
                vAxis: {minValue: min}
            }
        };

        const chart_followers = new google.visualization.AreaChart(document.getElementById('chart_followers'));
        chart_followers.draw(data, options(minFollowers));

        data = google.visualization.arrayToDataTable(arrayMedia);

        const chart_media = new google.visualization.AreaChart(document.getElementById('chart_media'));
        chart_media.draw(data, options(minMedia));

        data = google.visualization.arrayToDataTable(arrayER);

        const chart_er = new google.visualization.AreaChart(document.getElementById('chart_er'));
        chart_er.draw(data, options(minER));

        data = google.visualization.arrayToDataTable(arrayFollowing);

        const chart_following = new google.visualization.AreaChart(document.getElementById('chart_following'));
        chart_following.draw(data, options(minFollowing));

        const x2 = new XMLHttpRequest();
        x2.open("GET", "last-post?id=" + id, true);
        x2.onload = function () {
            const response = JSON.parse(x2.responseText);

            const dataLC = new google.visualization.DataTable();
            dataLC.addColumn('string', 'Data');
            dataLC.addColumn('number', 'Лайки');
            dataLC.addColumn('number', 'Комментарии');

            let like_count = 0;
            let comment_count = 0;
            let l = 0;
            let c = 0;
            let h = 0;
            let str = '';
            for (let i = 0; i < response.length; i++) {
                const dt = new Date(response[i]['created_at'] * 1000);
                like_count = response[i]['like_count'] === null ? 0 : response[i]['like_count'];
                comment_count = response[i]['comment_count'] === null ? 0 : response[i]['comment_count'];
                if (i === 0){ // первые лайки
                    l = like_count;
                    c = comment_count;
                    h = dt.getHours();
                    if (h.length < 2) str = '0' + h + ':00';
                    else str = h + ':00';
                    dataLC.addRow([str, response[i]['like_count'], response[i]['comment_count']]);
                } else {
                    let like = like_count - l;
                    if (like < 0) like = 0;
                    let com = comment_count - c;
                    if(com < 0) com = 0;
                    l = like_count;
                    c = comment_count;
                    h++;
                    if (h === 24) {
                        str = formatDay(dt);
                        h = 0;
                    } else {
                        if (h.length < 2) str = '0' + h + ':00';
                        else str = h + ':00';
                    }

                    dataLC.addRow([str, like, com]);
                }
                if (i > 23) break;
            }

            const options = {
                width: 900,
                height: 350,
                colors: ['#069BC5', '#97B962'],
                legend: {position: 'top', alignment: 'center', maxLines: 1}
            };

            if(document.getElementById('like-last-post') !== null && document.getElementById('like-last-post') !== undefined && like_count !== null)
                document.getElementById('like-last-post').innerText = like_count.toString();
            if(document.getElementById('comment-last-post') !== null && document.getElementById('comment-last-post') !== undefined && comment_count !== null)
                document.getElementById('comment-last-post').innerText = comment_count.toString();

            if (document.getElementById('columnchart_lc') !== null && document.getElementById('columnchart_lc') !== undefined){
                const columnchart_lc = new google.visualization.ColumnChart(document.getElementById('columnchart_lc'));
                columnchart_lc.draw(dataLC, options);
            }
        };
        x2.send(null);

        const x3 = new XMLHttpRequest();
        x3.open("GET", "stat-post?id=" + id, true);
        x3.onload = function () {
            let like_count = 0;
            let comment_count = 0;
            let taken_at = 0;
            const response = JSON.parse(x3.responseText);
            const dataNow = Math.floor(Date.now() / 1000);
            const dataMaxMouth = getMaxData(dataNow * 1000);
            let currMouth = dataMaxMouth.getMonth() + 1;
            let currYear = dataMaxMouth.getFullYear();
            const maxMouthTimestamp = dataMaxMouth.getTime() / 1000;
            const dataLike = new google.visualization.DataTable();
            dataLike.addColumn('string', 'Data');
            dataLike.addColumn('number', 'Лайки');

            const dataComment = new google.visualization.DataTable();
            dataComment.addColumn('string', 'Data');
            dataComment.addColumn('number', 'Комментарии');

            let arrLike = [];
            let arrComment = [];
            const arrPeriod = [];

            // 12 - константа означающая 12 недель/месяцев
            for (let j = 1; j < 6; j++){
                const period = getMaxDay(currYear, currMouth - 1) * 60 * 60 * 24;
                arrPeriod.push(period);
                let str = '', like = 0, comment = 0, nLike = 0, nComm = 0;
                for (let i = 0; i < response.length; i++) {
                    like_count = response[i]['like_count'] === null ? 0 : response[i]['like_count'];
                    comment_count = response[i]['comment_count'] === null ? 0 : response[i]['comment_count'];
                    taken_at = response[i]['taken_at'];
                    if (j === 1){
                        if ((maxMouthTimestamp - period * j)  < taken_at){
                            like += like_count;
                            comment += comment_count;
                            nLike++;
                            nComm++;
                        }
                    } else {
                        const start = GetStart(arrPeriod, j);
                        const finish = GetFinish(arrPeriod, j);
                        if ((maxMouthTimestamp - start) < taken_at && taken_at < (maxMouthTimestamp - finish)){
                            like += like_count;
                            comment += comment_count;
                            nLike++;
                            nComm++;
                        }
                    }
                }
                str = getMoth_rus(currMouth) + ' ' + currYear;
                let like_avg_2 = Math.round(like / nLike);
                if (isNaN(like_avg_2)) like_avg_2 = 0;
                arrLike.push([str, like_avg_2]);

                let comm_avg_2 = Math.round(comment / nComm);
                if (isNaN(comm_avg_2)) comm_avg_2 = 0;

                arrComment.push([str, comm_avg_2]);

                currMouth--;
                if (currMouth === 0){
                    currYear--;
                    currMouth = 12;
                }
            }

            arrLike = arrLike.reverse();
            arrComment = arrComment.reverse();
            dataLike.addRows(arrLike);
            dataComment.addRows(arrComment);

            const options = function (color) {
                return {
                    colors: [color],
                    legend: {position: 'none'}
                }
            };

            if (document.getElementById('chart_like') !== null && document.getElementById('chart_like') !== undefined){
                const chart_like = new google.visualization.ColumnChart(document.getElementById('chart_like'));
                chart_like.draw(dataLike, options('#069BC5'));
            }


            if (document.getElementById('chart_comment') !== null && document.getElementById('chart_comment') !== undefined){
                const chart_comment = new google.visualization.ColumnChart(document.getElementById('chart_comment'));
                chart_comment.draw(dataComment, options('#97B962'));
            }

        };
        x3.send(null);
    };
    x.send(null);

    const y = new XMLHttpRequest();
    y.open("GET", "audience?id=" + id, true);
    y.onload = function () {
        let response = JSON.parse(y.responseText);
        const array1 = [
            ["reach", "Count"]
        ];
        const array2 = [
            ["audience", "Count"]
        ];
        const array3 = [
            ["popularityOfFollowers", "Count"]
        ];
        const array4 = [
            ["popularityOfFollowing", "Count"]
        ];

        for (let i = 0; i < response.length; i++) {
            let type = response[i].type;
            if (type === "reach"){
                let name = response[i].name_ru;
                let value = response[i].value;
                array1.push([name, value]);
            }
            if (type === "audience"){
                let name = response[i].name_ru;
                let value = response[i].value;
                array2.push([name, value]);
            }
            if (type === "popularityOfFollowers"){
                let name = response[i].name;
                let value = response[i].value;
                array3.push([name, value]);
            }
            if (type === "popularityOfFollowing"){
                let name = response[i].name;
                let value = response[i].value;
                array4.push([name, value]);
            }
        }

        let options = {
            colors: ['#069BC5', '#97B962', '#E1545A', '#8E6FAB'],
            legend: {position: 'bottom'}
        };

        let data = google.visualization.arrayToDataTable(array1);

        let chart = new google.visualization.PieChart(document.getElementById('pie_reach'));

        chart.draw(data, options);

        data = google.visualization.arrayToDataTable(array2);

        chart = new google.visualization.PieChart(document.getElementById('pie_audience'));

        chart.draw(data, options);

        options = {
            legend: {position: 'none'},
            colors: ['#069BC5']
        };

        if (document.getElementById('chart_popularityOfFollowers') !== null && document.getElementById('chart_popularityOfFollowers') !== undefined){
            data = google.visualization.arrayToDataTable(array3);
            chart = new google.visualization.BarChart(document.getElementById('chart_popularityOfFollowers'));
            chart.draw(data, options);
        }

        if (document.getElementById('chart_popularityOfFollowing') !== null && document.getElementById('chart_popularityOfFollowing') !== undefined){
            data = google.visualization.arrayToDataTable(array4);
            chart = new google.visualization.BarChart(document.getElementById('chart_popularityOfFollowing'));
            chart.draw(data, options);
        }
    };
    y.send(null);
}

function findGetParameter(parameterName) {
    let result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

/**
 * @return {number}
 */
function GetStart(period, j) {
    let p = 0;
    for (let i = 0; i < period.length; i++) {
        if (j > i){
            p += period[i];
        } else {
            break;
        }
    }
    return p;
}

/**
 * @return {number}
 */
function GetFinish(period, j) {
    let p = 0;
    for (let i = 0; i < period.length; i++) {
        if (j - 1 > i){
            p += period[i];
        } else {
            break;
        }
    }
    return p;
}

/**
 * @return {string}
 */
function getMoth_rus(month) {
    switch (month)
    {
        case 1:
            month = 'Янв.';
            break;
        case 2:
            month = 'Фев.';
            break;
        case 3:
            month = 'Мар.';
            break;
        case 4:
            month = 'Апр.';
            break;
        case 5:
            month = 'Май';
            break;
        case 6:
            month = 'Июнь';
            break;
        case 7:
            month = 'Июль';
            break;
        case 8:
            month = 'Авг.';
            break;
        case 9:
            month = 'Сент.';
            break;
        case 10:
            month = 'Окт.';
            break;
        case 11:
            month = 'Ноя.';
            break;
        case 12:
            month = 'Дек.';
            break;
        default:
            month = 'Ошибка';
    }
    return month;
}

function formatDay(date) {
    const d = new Date(date);
    let month = (d.getMonth() + 1),
        day = '' + d.getDate();

    if (day.length < 2) day = '0' + day;

    month = getMoth_rus(month);

    return [day, month].join(' ');
}

getMaxDay = function(y, m) {
    if (m === 1) {
        return y%4 || (!(y%100) && y%400 ) ? 28 : 29;
    }
    return m===3 || m===5 || m===8 || m===10 ? 30 : 31;
};

function getMaxData(date) {
    const d = new Date(date),
        m = (d.getMonth() + 1),
        y = '' + d.getFullYear();

    //new Date(2011, 0, 1, 0, 0, 0, 0); // // 1 января 2011, 00:00:00
    const day = getMaxDay(y, m - 1);
    return new Date(y, m - 1, day, 23, 59, 59, 0);
}