// переменная хранить идентификатор инстаграмма выбранный пользователем
var instagramId = 0;
var calendar = $('#calendar');
var proxy_settings = $("#proxy-settings");
var post_modal = $("#post-modal");
var event_modal = $("#event-modal");
var event_form = $("#event-form");
var datebox = $("#datetimepicker");


$(function () {
    $('[data-toggle="popover"]').popover()
});

$("#enabled-proxy").on("change", function () {
    proxy_settings.toggle();

    if ($("#enabled-proxy").prop("checked")) {
        proxy_settings.find("input").removeAttr("disabled");
    } else {
        proxy_settings.find("input").attr("disabled", "disabled");
        proxy_settings.find(".form-group").removeClass("has-error");
    }
});

function getTimezone() {
    var tz = $("#timezone").find("option:selected").text();
    var found = tz.match(/\(GMT\s\+[^\)]+\)\s([^$]+)/i);
    if (found === null || found.length < 2) {
        alert('error timezone');
        return false;
    }
    return found[1];
}

$("#menu-settings-modal").on("click", function () {
    var showDate = function () {
        var date = new Date();
        var currentDate = moment.tz(date.getTime(), getTimezone()).format("YYYY-MM-DD HH:mm:ss");
        var u = moment.tz(date.getTime(), getTimezone()).unix();
        $("#current_date").text(u);
    };

    setInterval(showDate, 1000);
    showDate();

    $("#settings-modal").modal("show");
});

$("#settings-form").submit(function(e) {
    e.preventDefault();
    //сохранение часового пояса
    $("#settings-modal").modal("hide");
    return false;
});

/**
 * Создаём календарь
 *
 * @arrayEvents {массив элементов} пример:
 *  var arrayEvents =    [
 {
     "title": "0",
     "start": "2017-04-03 12:00:00",
     "end": "2017-04-03 13:00:00"
 },
 {
     "title": "0",
     "start": "2017-04-13 15:00:00",
     "end": "2017-04-13 16:00:00"
 },
 {
     "title": "0",
     "start": "2017-04-25 07:00:00",
     "end": "2017-04-25 08:00:00"
 },
 {
     "title": "0",
     "start": "2017-04-25 07:00:00",
     "end": "2017-04-25 08:00:00"
 }
 ];
 */
function createCalendar(arrayEvents) {
    if (calendar.attr('id') === 'calendar') {
        var initialLocaleCode = 'ru';
        calendar.fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            defaultView: 'month',
            locale: initialLocaleCode,
            buttonIcons: false,
            weekNumbers: true,
            navLinks: true,
            editable: true,
            eventLimit: true,
            selectable: true,
            selectHelper: true,
            droppable: true,
            select: function(start, end) {
                post_modal.off('submit');
                post_modal.submit({start: start, end: end}, function(e) {
                    if (!e.isDefaultPrevented()) {
                        var eventTitle = "Upload photo"; //$("#event_title").val();
                        var eventData = {
                            title: eventTitle,
                            start: e.data.start,
                            end: e.data.end,
                            color: "#FF8D0C",
                            textColor: "#878078"
                        };

                        //сюда можно прописать отправку созданного события на сервер
                        //при получении ответа с сервера также можно прописать id события

                        calendar.fullCalendar('renderEvent', eventData, true);
                        calendar.fullCalendar('unselect');
                        post_modal.modal('hide');
                    }

                    e.preventDefault();
                    return false;
                });

                post_modal.find(".btn-danger").hide();
                post_modal.trigger("reset");
                post_modal.validator();
                // todo: необходимо передать текущую дату в окно думаю тут, так как именно здесь происходит открытие
				var start_data = moment(start).format('DD.MM.YYYY');
				datebox.datetimepicker({
					value: start_data
				});
                post_modal.modal('show');
            },
            events: arrayEvents,
            eventColor: '#FF8D0C',
            eventTextColor: '#878078',
            eventClick: function(event, element) {
                event_modal.off('submit');
                event_modal.submit(function(e) {
                    if (!e.isDefaultPrevented()) {
                        event.title = $("#event_title").val();

                        //сюда можно прописать отправку отредактированного события на сервер

                        calendar.fullCalendar('updateEvent', event);
                        calendar.fullCalendar('unselect');
                        event_modal.modal('hide');
                    }

                    return false;
                });
               event_form.find(".btn-danger").off("click").on("click", function () {
                    calendar.fullCalendar('removeEvents', function (eventObject) {

                        //сюда можно прописать отправку удаления события на сервер

                        return event.title === eventObject.title;
                    });

                    event_modal.modal('hide');
                });

               event_form.find(".btn-danger").show();
               event_form.trigger("reset");
               event_form.validator({disable: false});
                event_modal.off('shown.bs.modal').on('shown.bs.modal', function () {
                    $("#event_title").val(event.title).focus();
                });
                post_modal.modal('show');
            },
            eventDrop: function(event, delta, revertFunc) {

                //сюда можно прописать отправку перемещении события на сервер
                //при перемещении обновляется только начало события - start

                console.log(event.start.format());
            },
            eventResize: function (event, delta, revertFunc) {

                //сюда можно прописать отправку ресайзе события на сервер
                //при ресайзе обновляется только конец события - end

                console.log(event.end.format());
            }
        });
        calendar.fullCalendar('refresh');
        calendar.fullCalendar('refetchEvents');
    }
}

// var arrayEvents =    [
//         {
//             "title": "0",
//             "start": "2017-04-03 12:00:00",
//             "end": "2017-04-03 13:00:00"
//         },
//         {
//             "title": "0",
//             "start": "2017-04-13 15:00:00",
//             "end": "2017-04-13 16:00:00"
//         },
//         {
//             "title": "0",
//             "start": "2017-04-25 07:00:00",
//             "end": "2017-04-25 08:00:00"
//         },
//         {
//             "title": "0",
//             "start": "2017-04-25 07:00:00",
//             "end": "2017-04-25 08:00:00"
//         }
//     ];
//
// createCalendar(arrayEvents);
var sideBar = document.getElementById('side-bar');
if (sideBar !== null) {
    GetInstagramAccounts(sideBar);
}

/**
 * Создание списка инстаграмм аккаунтов в виде html блоков
 *
 * @sideBar {HTMLElement} x Число для возведения в степень.
 * @data {string} строка полученная с rest сервера
 * @return {number} идентификатора первого объекта в списке
 */
function CreateListInstagramAccounts(sideBar, data) {
    // шаблон блока который нужно вставить в блок с id = slide-bar, таких блоков может быть множество, что бы заполнить
    // нужно выполнить rest запрос к API, первый элемент user-active
    var templateAccountView = '<div class="user-panel{0}" id="{3}">'+
            '<div class="pull-left image">'+
                '<img src="{4}" class="img-circle" alt="User Image">'+
            '</div><div class="pull-left info">'+
                '<p>{1}</p>'+
                '<span>{2}</span>' +
            '</div></div>';
    var accountsList = '';
    var accountItem = '';
    var i = 1;
    var firstId = 0;

    var j = JSON.parse(data);

    j.forEach(function (item) {
        if (i === 1){
            accountItem = templateAccountView.replace('{0}',' user-active');
            firstId = item['id'];
        }else {
            accountItem = templateAccountView.replace("{0}","");
        }
        var id = item['id'];
        accountItem = accountItem.replace('{1}', item['fullname']);
        accountItem = accountItem.replace('{2}', item['username']);
        accountItem = accountItem.replace('{4}', item['foto']);
        accountItem = accountItem.replace('{3}', id);
        accountsList += accountItem;

        i++;
    });
    var buttonAddAccount = '<a href="#" id="add-account" aria-label="Left Align" data-toggle="modal" class="btn btn-add-accounts"><span class="glyphicon glyphicon-plus"></span>Add more accounts</a>';
    sideBar.innerHTML = accountsList + buttonAddAccount;
    $("#add-account").on("click", function() {
        $("#account-form").trigger("reset");
        proxy_settings.find("input").attr("disabled", "disabled");
        proxy_settings.hide();
        $("#account-modal").modal("show");
        $("#btn-add-inst").click(function () {
            // todo: необходимо проверить заполненность атрибутов
            var login = $("#instagram_login").val();
            if (login.length < 1)
                return;
            var fd = new FormData();
            fd.append("login", login);
            fd.append("password", $("#instagram_password").val());
            fd.append("proxy_ip", $("#proxy_ip").val());
            fd.append("proxy_port", $("#proxy_port").val());
            fd.append("proxy_login", $("#proxy_login").val());
            fd.append("proxy_password", $("#proxy_password").val());

            var xhr = new XMLHttpRequest();
            xhr.upload.onload = function() {
                // действие после загрузки.
                // скрыть окно, добавить пункт в меню
                $("#account-modal").modal("hide");
                GetInstagramAccounts(sideBar);
            };

            xhr.upload.onerror = function() {
                alert( 'Произошла ошибка при загрузке данных на сервер!' );
            };

            xhr.open("post", "add-inst", true);
            xhr.send(fd);
        })
    });

    $(".user-panel").on("click", function() {
        instagramId = $(this).attr("id");
        console.log("Id: " + instagramId);
        $(".user-panel").removeClass("user-active");
        $("#" + instagramId).addClass("user-active");
        calendar.fullCalendar('destroy');
        GetTasks(instagramId);
    });
    return firstId;
}

/**
 * Подключаемся по rest и получаем текст в котором хранятся аккаунты инстаграмма
 *
 * @sideBar {HTMLElement} - блок в который встраиваем список
 */
function GetInstagramAccounts(sideBar) {
    var x = new XMLHttpRequest();
    x.open("GET", "/inst/", true);
    x.onload = function (){
        var firstId = CreateListInstagramAccounts(sideBar, x.responseText);
        if (firstId === 0)
        {
            console.log("Index error!");
            return;
        }
        var calendar = document.getElementById('calendar');
        if (calendar !== null)
        {
            instagramId = firstId;
            GetTasks(firstId);
        }
    };
    x.send(null);
}

/**
 * Подключаемся по rest и получаем список задач для данного аккаунта
 *
 * @sideBar {HTMLElement} x Число для возведения в степень.
 * @id {number} ид аккаунта
 */
function GetTasks(id) {

    var req = "/tasks?id=" + id;
    var x = new XMLHttpRequest();
    x.open("GET", req, true);
    x.onload = function (){
        var j = JSON.parse(x.responseText);
        createCalendar(j);
    };
    x.send(null);
}

/**
 * Получаем время в секундах
 *
 * @d {string} дата в виде шабона dd.mm.YYYY, пример 30.05.2017
 * @t {string} время в виде шаблна H:m, пример 17:15
 * @return {number} время в секундах
 */
 function getTimestamp(d, t){
            var p_d = d.split('.');
            // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
            var p_t = t.split(':');
            return (new Date(p_d[2], p_d[1]-1, p_d[0], p_t[0], p_t[1]).getTime()) / 1000; // Note: months are 0-based
}
/**
 * var eventArray = [{
     "title": "0",
     "start": "2017-04-03 12:00:00",
     "end": "2017-04-03 13:00:00"
 }];
 * @param event - array of event
 */
function addEvent(event) {
    if (calendar.attr('id') === 'calendar') {
        calendar.fullCalendar('addEventSource', event);
        calendar.fullCalendar('refetchEvents');
    }
}

/**
 * Случайное значение в интервале
 *
 * @min {number} 
 * @max {number} 
 * @return {number} 
 */
function randomIntFromInterval(min, max) {
        return Math.floor(Math.random()*(max-min+1)+min);
}
//add post modal
(function () {
    var post_form = $("#post-form");
    
    $(".tab-caption").on("click", function () {
        if ($("#file").val() === "") {
            return false;
        }

        $(".save, .back").show();
    });

    $(".tab-upload").on("click", function () {
        $(".save, .back").hide();
    });

    $(".back").on("click", function () {
        $(".tab-upload").click();

        return false;
    });

    if (datebox.attr('id') === 'datetimepicker') {
        $.datetimepicker.setLocale('ru');

        datebox.datetimepicker({
            timepicker: false,
            format: 'd.m.Y'
        });

        function isInvalidTimeRange() {
            var startTime = $('#start_time').val();
            var startTimeParts = startTime.split(":");

            var endTime = $('#end_time').val();
            var endTimeParts = endTime.split(":");

            if (startTimeParts.length != 2 || endTimeParts.length != 2) {
                return true;
            }

            var startTimeMinutes = parseInt(startTimeParts[0]) * 60 + parseInt(startTimeParts[1]);
            var endTimeMinutes = parseInt(endTimeParts[0]) * 60 + parseInt(endTimeParts[1]);

            return startTimeMinutes >= endTimeMinutes;
        }

        $('#start_time').datetimepicker({
            datepicker: false,
            format: 'H:i',
            step: 15
        });

        $('#end_time').datetimepicker({
            datepicker: false,
            format: 'H:i',
            step: 15
        });

        $(".add-post").on("click", function () {
            post_form.trigger("reset");
            $(".tab-upload").click();
            post_modal.modal("show");
        });

        $(".input-group-addon").on("click", function () {
            datebox.focus();
        });
    }

    post_form.validator({
        disable: false,
        delay: 100,
        custom: {
            'time-range': function () {
                return isInvalidTimeRange();
            }
        }
    });

    function getFile() {
        var files = document.getElementById('file').files;
        return files[0];
    }

    var file = document.getElementById('file');
    if (file !== null) {
        file.onchange = function(){
            createPreview(getFile());
            $(".tab-caption").click();
        };
    }

    function resize(img, maxWidth, maxHeight) {
        var width = img.width;
        var height = img.height;

        if (width > maxWidth) {
            height *= maxWidth / width;
            width = maxWidth;
        }
        if (height > maxHeight) {
            width *= maxHeight / height;
            height = maxHeight;
        }

        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        canvas.className = "center-block";
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);

        return canvas;
    }

    function createPreview(file) {
        if (typeof FileReader === "undefined" || !(/image/i).test(file.type)) {
            return false;
        }

        var img = document.createElement("img");
        img.onload = function() {
            var preview = document.getElementById('preview');
            preview.innerHTML = '';
            preview.appendChild(resize(img, 100, 100));
        };

        var reader = new FileReader();
        reader.onload = function(e) {
            img.src = e.target.result;
        };
        reader.readAsDataURL(file);
    }
    
    function createDataForEvent(day, time){
        return  day.split(".").reverse().join("-") + " " + time + ":00";
    }

    post_form.submit(function (e) {
        if (!e.isDefaultPrevented()) {
            var file = getFile();
            var img = document.createElement("img");
            img.onload = function() {
                var canvas = resize(img, 1080, 1350);
                var dataUrl = canvas.toDataURL(file.type);
                dataUrl = dataUrl.replace(/data\:image\/[^\;]*\;base64\,/, '');
                var fd = new FormData();
                fd.append("image", dataUrl);
                fd.append("message", $("#message").val());
                var d = datebox.val(); //строка в формате 2017.04.03
                var start = $("#start_time").val(); // строка в формате "17:55"
                var end = $("#end_time").val();
                var t_start = getTimestamp(d, start);
                var t_end = getTimestamp(d, end);
                var task_time = randomIntFromInterval(t_start, t_end);
                fd.append("start_time", t_start);
                fd.append("end_time", t_end);
                fd.append("task_time", task_time);
                fd.append("id_instagram", instagramId);
                var xhr = new XMLHttpRequest();
                xhr.upload.onload = function() {
                    post_modal.modal("hide");
                    // получим список задач и перерисуем календарь
                    //"2017-04-25 07:00:00"
                    var st = createDataForEvent(d, start);
                    var en = createDataForEvent(d, end);
                    var event = [{
                        title: "Upload photo",
                        start: st,
                        end: en
                    }];
                    // event = [{
                    //     "title": "0",
                    //     "start": "2017-06-05 12:00:00",
                    //     "end": "2017-06-05 13:00:00"
                    // }];
                    addEvent(event);
                };
                xhr.upload.onerror = function() {
                    alert( 'Произошла ошибка при загрузке данных на сервер!' );
                };
                xhr.open("post", "photo", true);
                xhr.setRequestHeader("X-File-Name", file.name);
                xhr.setRequestHeader("X-File-Size", file.size);
                xhr.setRequestHeader("X-File-Type", file.type);
                xhr.send(fd);
            };
            var reader = new FileReader();
            reader.onload = function(e) {
                img.src = e.target.result;
            };
            reader.readAsDataURL(file);
        }

        return false;
    });

    $(".fa-upload").on("click", function () {
        $("#file").click();
    });
})();



