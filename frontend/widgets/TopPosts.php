<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use common\models\StatPost;

/*
 * <?= TopPosts::widget(['top_photo' => $account->top_photo, 'type' => 'like_count']) ?>
 */
class TopPosts extends Widget
{
    /**
     * @var array|StatPost[]
     */
    public $top_photo;
    public $type;

    private $result;
    private $templateType;

    public function init(){
        parent::init();
        $checkType = False;
        $this->result =  '';
        $this->templateType = ['like_count', 'comment_count'];
        if($this->top_photo === null || $this->type === null){
            $this->result =  'error';
        }else{
            foreach ($this->templateType as $item){
                if ($this->type == $item)
                    $checkType = True;
            }
            if ($checkType === False)
            {
                $this->result =  'error';
            } else {
                $this->create();
            }
        }
    }

    public function run(){
        return $this->result;
    }

    private function create()
    {
        $top_photo = $this->top_photo;
        $i = 0;
        if (isset($top_photo)){
            foreach ($top_photo as $item){
                if ($i > 5) break;
                if (empty($item->url)) continue;
                if ($i === 0){
                    echo Html::beginTag('div', ['class' => 'col-sm-8']);
                    echo Html::beginTag('div', ['class' => 'top-post-widget', 'style'=>'width: 380px;height: 380px;']);
                    echo Html::a(Html::img(Html::encode($item->url), ['class' => 'first-top-foto']), null,
                        ["class" => "showModalPost", "data-url"=> $item->url, "data-content" => $item->caption,
                            "data-like" => $item->like_count, "data-comm" => $item->comment_count, "data-code" => $item->code]);
                } elseif ($i === 1) {
                    echo Html::beginTag('div', ['class' => 'col-sm-4']);
                    echo Html::beginTag('div', ['class' => 'top-post-widget', 'style'=>'width: 175px;height: 175px;']);
                    echo Html::a(Html::img(Html::encode($item->url), ['class' => 'top-foto']), null,
                        ["class" => "showModalPost", "data-url"=> $item->url, "data-content" => $item->caption,
                            "data-like" => $item->like_count, "data-comm" => $item->comment_count, "data-code" => $item->code]);
                } elseif ($i === 2){
                    echo Html::beginTag('div', ['class' => 'top-post-widget', 'style'=>'width: 175px;height: 175px;']);
                    echo Html::a(Html::img(Html::encode($item['url']), ['class' => 'top-foto']), null,
                        ["class" => "showModalPost", "data-url"=> $item->url, "data-content" => $item->caption,
                            "data-like" => $item->like_count, "data-comm" => $item->comment_count, "data-code" => $item->code]);
                } else {
                    echo Html::beginTag('div', ['class' => 'col-sm-4']);
                    echo Html::beginTag('div', ['class' => 'top-post-widget', 'style'=>'width: 175px;height: 175px;']);
                    echo Html::a(Html::img(Html::encode($item->url), ['class' => 'top-foto']), null,
                        ["class" => "showModalPost", "data-url"=> $item->url, "data-content" => $item->caption,
                            "data-like" => $item->like_count, "data-comm" => $item->comment_count, "data-code" => $item->code]);
                }
                echo Html::beginTag('div', ['class' => 'top-post-info']);
                switch ($this->type){
                    case 'like_count':
                        $icon = 'fa fa-thumbs-up';
                        break;
                    case 'comment_count':
                        $icon = 'fa fa-comment';
                        break;
                    default:
                        $icon = 'fa fa-comment';
                }
                echo Html::tag('i', '&nbsp;' . Html::encode($item[$this->type]),
                    ['class' => $icon, 'style' => 'font-size: 20px;']);
                echo Html::endTag('div');
                echo Html::endTag('div');
                if ($i != 1) echo Html::endTag('div');
                $i++;
            }
        }
    }
}