<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 31.08.2017
 * Time: 20:45
 */
namespace frontend\utility;
use yii\helpers\Html;

class HtmlBlocks
{
    public static function getSign($diff){
        if ($diff > 0)
        {
            $sign = "+";
            return $sign . $diff;
        }
        return '' . $diff;
    }

    /**
     * Заполенение ячейки таблицы на главной страницы с отображением или нет разницы
     * @param int $diff
     * @param int $count
     * @return string
     */
    public static function IndexCellNum($diff, $count)
    {
        $block = '<div><p style="margin-top: 10px;">%s</p><div style="text-align: center; margin-top: -10px;">'
                .'<span class="diff-numb" style="color: %s;">%s%s</span> </div></div>';
        $block2 = '<div><p style="margin-top: 10px;">%s</p></div>';
        if ($diff < 0) {
            $sign = "";
            $color = "#FB5557";
        } elseif ($diff == $count){
            if ($count == 0) $count = '-';
            return sprintf($block2, $count);
        } elseif( $diff > 0) {
            $sign = "+";
            $color = "#66b821";
        } else {
            if ($count == 0) $count = '-';
            return sprintf($block2, $count);
        }
        return sprintf($block, $count, $color, $sign,  $diff);
    }

    public static function InstUrl($username, $text = null, $options = [])
    {
        $urlTemplate = 'https://www.instagram.com/%s/';
        $url = sprintf($urlTemplate, $username);
        if (empty($text)){
            return Html::a($username, $url, $options);
        } else {
            return Html::a($text, $url, $options);
        }
    }

    public static function InstPostUrl($code, $text, $options = [])
    {
        $urlTemplate = 'https://www.instagram.com/p/%s/';
        $url = sprintf($urlTemplate, $code);
        return Html::a($text, $url, $options);
    }
}