<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class PostingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/baguetteBox.min.css',
        'css/gallery-clean.css',
        'css/gallery-container.css',
    ];

    public $js = [
        'js/baguetteBox.min.js',
        'js/baguetteBox.run.js'
    ];
    public $depends = [
        'frontend\assets\AppAsset'
    ];
}