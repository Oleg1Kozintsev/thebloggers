<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class JqCropboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/cropbox';
    public $js = [
        'jquery.cropbox.min.js',
    ];
    public $css = [
        'jquery.cropbox.css',
    ];
}