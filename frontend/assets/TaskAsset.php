<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class TaskAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/jasny-bootstrap.min.css',
        'vendor/fullcalendar-3.4.0/fullcalendar.min.css',
        'vendor/datetimepicker/jquery.datetimepicker.min.css',
        'vendor/font-awesome-4.7.0/css/font-awesome.min.css',
        'css/tasks.min.css'
    ];

    public $js = [
        'js/jasny-bootstrap.min.js',
        'js/validator.min.js',
        'vendor/fullcalendar-3.4.0/lib/moment.min.js',
        'vendor/fullcalendar-3.4.0/fullcalendar.min.js',
        'vendor/fullcalendar-3.4.0/locale-all.js',
        'js/moment-timezone-with-data.min.js',
        'vendor/datetimepicker/jquery.datetimepicker.full.min.js',
        'js/tasks.min.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset'
    ];
}
