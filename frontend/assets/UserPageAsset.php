<?php
namespace frontend\assets;

use yii\web\AssetBundle;


class UserPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/userpage.css'
    ];

    public $js = [
        'https://www.gstatic.com/charts/loader.js',
        'js/userpage.js'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
        'frontend\assets\JqCropboxAsset'
    ];
}