<?php

namespace frontend\assets;

use yii\web\AssetBundle;



class SideBarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/jasny-bootstrap.min.css',
        'css/side-bar.css'
    ];

    public $js = [
        'js/posting.min.js',
        'js/jasny-bootstrap.min.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset'
    ];
}