<?php
namespace frontend\models;

use yii\base\Model;
/**
 * This is the model class for table "instagram".
 *
 * @property \yii\web\UploadedFile $imageFile
 * @property string $caption
 */
class PostingForm extends Model
{

    public $imageFile;
    public $caption;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            ['caption', 'default'],
        ];
    }

    public function save(){

    }

    // todo: сгенерировать случайно имя, сохранить это имя в переменную, для записи в базу данных
    public function upload()
    {
        if ($this->validate()) {
            $fileName = uniqid().'.' . $this->imageFile->extension;
            $this->imageFile->saveAs('uploads/' . $fileName);
            // сохраняем данные в базу
            return true;
        } else {
            return false;
        }
    }
}