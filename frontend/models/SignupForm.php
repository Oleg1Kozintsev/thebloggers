<?php
namespace frontend\models;

use common\models\Invite;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password2;
    public $captcha;
    public $invite;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'string', 'min' => 2, 'max' => 45],
            [['password', 'password2'], 'string', 'min' => 2, 'max' => 32],
            ['email', 'email'],
            ['password', 'required'],
            ['password2', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны сопадать.'],
            ['invite', 'required'],
            ['invite', 'validateInvite'],
        ];
    }

    public function validateInvite($attribute, $params)
    {
        if (!Invite::validateInvite($this->$attribute)) {
            $this->addError($attribute, 'Указан не верный инвайт.');
        }
    }


    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'username'  => \Yii::t('app','Username'),
            'email'     => \Yii::t('app','Email'),
            'password'  => \Yii::t('app','Password'),
            'password2' => \Yii::t('app','password2'),
            'invite'    => \Yii::t('app','Invite'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->status = User::STATUS_ACTIVE;
        $user->role = User::ROLE_USER;
        $user->generateAuthKey();
        $user->generateEmailConfirmToken();

        if ($user->save()) {
//            Yii::$app->mailer->compose('@app/modules/user/mails/emailConfirm', ['user' => $user])
//                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
//                    ->setTo($this->email)
//                    ->setSubject('Email confirmation for ' . Yii::$app->name)
//                    ->send();
            return $user;
        }
        return null;
    }
}
