<?php
namespace frontend\models;

use yii\base\Model;
use common\models\Inst;

class AccountModalForm extends Model
{
    public $instagram_login;
    public $instagram_password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['instagram_login', 'string', 'min' => 2, 'max' => 45],
            ['instagram_password', 'string', 'min' => 2, 'max' => 32],
            ['instagram_password', 'required'],
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            // do save in db in model Inst
            return true;
        } else {
            return false;
        }
    }
}