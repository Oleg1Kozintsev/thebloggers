<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use cybercog\yii\googleanalytics\widgets\GATracking;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="icon" href="favicon.ico">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= GATracking::widget([
        'trackingId' => 'UA-106855014-1',
    ]) ?>
</head>
<body>
<?php $this->beginBody() ?>
    <nav id="w0" class="navbar navbar-default navbar-fixed-top">
        <div style="width: 93%; float: left; padding-top: 12px;">
            <a id="logo" href=" <?= Yii::$app->homeUrl ?>">The Bloggers</a><br>
            <span>Независимая, бесплатная, подробная статистика</span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php if (!Yii::$app->user->isGuest)
                {
                    echo "<li class=\"dropdown\">";
                    echo "<a id=\"drop1\" href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo Yii::$app->user->identity->email . " <span class=\"caret\"></span></a>";
                    echo "<ul class=\"dropdown-menu\">";
                    echo "<li><a href=\"#\" id=\"menu-settings-modal\">". \Yii::t('app','Settings')."</a></li>";
                    echo "<li class=\"divider\"></li>";
                    echo "<li>";
                    echo Html::beginForm(['/logout'], 'post', ['style' => 'padding-left: 10px;'])
                        . Html::submitButton(
                            \Yii::t('app','Logout'),
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm();
                    echo "</li>";
                    echo "</ul>";
                    echo "</li>";
                }
                ?>
            </ul>
        </div>
    </nav>
        <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
