<?php

/* @var $this yii\web\View */

/* @var $account common\models\cache\CacheStatsAccounts */

use frontend\utility\HtmlBlocks;

?>
<div class="row">
    <div class="col-md-4">
        <div class="dashboard-stat2 bordered dashboard-stat2-full">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup"><?= $account->er_followers ?>%</span>
                        <small>Engagement Rate (followers)</small>
                    </h3>
                </div>
                <div class="icon">
                    <i class="fa fa-star-o fa-3x" aria-hidden="true" style="font-size: 20px;"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: <?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%;"
                          class="progress-bar progress-bar-success red-haze"></span>
                </div>
                <div class="status">
                    <div class="status-title">За период (24 часа) <span
                                class="change-green"><?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%</span>
                    </div>
                    <div class="status-activity">
                        <h3 class="font-red-haze">
                            <span data-counter="counterup"><?= $account->er ?>%</span><span><small class="">ER (activity)</small></span>
                        </h3>
                        <small>Всего лайков и комментариев</small>
                    </div>
                    <div class="status-not-followers">
                        <h3 class="font-red-haze">
                            <span data-counter="counterup"><?= $account->er - $account->er_followers ?>%</span><span><small>ER (not followers)</small></span>
                        </h3>
                        <small>Возможно накрутка</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="dashboard-stat2 bordered dashboard-stat2-full">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup"><?= $account->lr_followers ?>%</span>
                        <small>Love Rate (followers)</small>
                    </h3>
                </div>
                <div class="icon">
                    <i class="fa fa-heart-o fa-3x" aria-hidden="true" style="font-size: 20px;"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: <?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%;"
                          class="progress-bar progress-bar-success red-haze"></span>
                </div>
                <div class="status">
                    <div class="status-title">За период (24 часа) <span
                                class="change-green"><?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%</span>
                    </div>
                    <div class="status-activity">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup"><?= $account->lr ?>%</span><span><small class="">LR (activity)</small></span>
                        </h3>
                        <small>Всего лайков</small>
                    </div>
                    <div class="status-not-followers">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup"><?= $account->lr - $account->lr_followers ?>%</span><span><small>LR (not followers)</small></span>
                        </h3>
                        <small>Возможно накрутка</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="dashboard-stat2 bordered dashboard-stat2-full">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup"><?= $account->tr_followers ?>%</span>
                        <small>Talk Rate (followers)</small>
                    </h3>
                </div>
                <div class="icon">
                    <i class="fa fa-comments-o fa-3x" aria-hidden="true" style="font-size: 20px;"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success green-sharp"></span>
                </div>
                <div class="status">
                    <div class="status-title">За период (24 часа) <span class="change-green">0.00%</span></div>
                    <div class="status-activity">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup"><?= $account->tr ?>%</span><span><small class="">TR (activity)</small></span>
                        </h3>
                        <small>Всего комментариев</small>
                    </div>
                    <div class="status-not-followers">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup"><?= $account->tr - $account->tr_followers ?>%</span><span><small>TR (not followers)</small></span>
                        </h3>
                        <small>Возможно накрутка</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

