<?php

/* @var $this yii\web\View */
/* @var $account common\models\cache\CacheStatsAccounts */

use yii\helpers\Html;
use frontend\utility\HtmlBlocks;
use common\utility\Datetime;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="caption-subject font-green-sharp"><strong>ПОСЛЕДНИЙ ПОСТ</strong></span>
            <span class="caption-helper">&nbsp;
                    от <?= Datetime::getDataTime($account->last_media_timestamp)?>
                </span>
        </h3>
        <div class = "action">
            <?= HtmlBlocks::InstPostUrl($account->last_media_code, 'Открыть', ["class" => "btn grey-salsa btn-circle btn-sm", "target" => "_blank"]) ?>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3 col-md-5">
                <?= Html::img($account->last_media_url, ['alt' => $account->last_media_caption, 'style' => 'width: 100%; display: block;']) ?>
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-6" style="text-align: center; font-size: 22px; color: #069bc5;"><i class="fa fa-thumbs-up" style="font-size: 20px;"></i>
                        &nbsp;<span id="like-last-post">0</span>
                    </div>
                    <div class="col-md-6" style="text-align: center; font-size: 22px; color: #97b962;"><i class="fa fa-comment" style="font-size: 20px;"></i>
                        &nbsp;<span id="comment-last-post">0</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-7">
                <div id="columnchart_lc" style="width: 600px; height: 400px;"></div>
            </div>
        </div>
    </div>
    <div class="panel-footer text-center">
        Динамика набора лайков и комментариев на последнем посте в течение суток после выкладки
    </div>
</div>