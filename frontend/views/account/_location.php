<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $locationForm common\models\LocationForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

?>

<div class="location">
    <?php Pjax::begin(['id' => 'location-upd']) ?>

    <h3 hidden="hidden" id="save-info">Сохранено</h3>

    <?php $form = ActiveForm::begin([
        'id' => 'location-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>

    <?= $form->field($locationForm, 'country')->textInput(['autofocus' => true]) ?>

    <?= $form->field($locationForm, 'city')->textInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <?= Html::submitButton(\Yii::t('app','Save'), ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
