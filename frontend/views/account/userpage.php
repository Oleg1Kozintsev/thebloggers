<?php

/* @var $this yii\web\View */
/* @var $account common\models\cache\CacheStatsAccounts */
/* @var $topFollower yii\data\ActiveDataProvider */
/* @var $topLike common\models\StatPost[] */
/* @var $topComment common\models\StatPost[] */
/* @var $locationForm common\models\LocationForm */

use yii\helpers\Html;
use frontend\utility\HtmlBlocks;
use frontend\widgets\TopPosts;
use frontend\assets\UserPageAsset;
use common\utility\Number;
use yii\bootstrap\Modal;


$this->title = 'About ' . $account->username;
$this->params['breadcrumbs'][] = $this->title;

UserPageAsset::register($this);

Modal::begin([
    'header' => '<h4> Просмотр поста</h4>',
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'post-modal',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static'],
    'closeButton' => ['id' => 'close-button'],
    'footer' => ' <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>'
]);
echo $this->render('_modalContent');
Modal::end();
?>
    <div class="account-user-info container-fluid">

        <div class="row row-flex well" style="margin-right: 0px; margin-left: 0px;">
            <div class="col-lg-6 col-md-4">
                <div class="row row-flex">
                    <div class="col-md-6 text-center">
                        <figure>
                            <?= Html::img($account->profilePicUrl, ['alt' => $account->fullname, 'class' => 'img-circle img-responsiv']) ?>
                        </figure>
                    </div>
                    <div class="col-md-6">
                        <h2><?= Html::encode($account->fullname) ?></h2>
                        <p><?= HtmlBlocks::InstUrl($account->username, null, ['target' => '_blank']) ?> </p>
                        <p><strong>Биография: </strong> <?= Html::encode($account->biography) ?> </p>
                        <?php
                        if (!Yii::$app->user->isGuest) {
                            echo $this->render('_location', ['locationForm' => $locationForm]);
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-8">
                <div class="row row-flex">
                    <div class="col-md-12 hidden-xs text-center" style="height: 35px;">
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong>
                                <?= Html::encode(Number::bd_nice_number($account->followers_count)) ?>
                            </strong>
                        </h3>
                        <p>
                            <small><?= \Yii::t('app', 'Followers') ?></small>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong> <?= Html::encode($account->media_count) ?> </strong></h3>
                        <p>
                            <small><?= \Yii::t('app', 'Media count') ?></small>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong> <?= Html::encode(Number::bd_nice_number($account->like_avg)) ?> </strong></h3>
                        <p>
                            <small><?= \Yii::t('app','Likes (avg)') ?></small>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong> <?= Html::encode(Number::bd_nice_number($account->comment_avg)) ?> </strong></h3>
                        <p>
                            <small>Комментариев (ср.)</small>
                        </p>
                    </div>
                </div>
            </div>

        </div>

        <?php if (isset($account->last_media_url) && $account->last_media_url !== 0 && strlen($account->last_media_url) > 5) {
            echo $this->render('_last-post', ['account' => $account]);
        } ?>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span
                                    class="caption-subject font-green-sharp"><strong>ТОП ПО ЛАЙКАМ</strong></span></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?= TopPosts::widget(['top_photo' => $topLike, 'type' => 'like_count']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="caption-subject font-green-sharp"><strong>ТОП ПО КОММЕНТАРИЯМ</strong></span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?= TopPosts::widget(['top_photo' => $topComment, 'type' => 'comment_count']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span
                                    class="caption-subject font-green-sharp"><strong>ПОДПИСЧИКОВ</strong></span></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="chart_followers" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span
                                    class="caption-subject font-green-sharp"><strong>ПОСТОВ</strong></span></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="chart_media" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <?php
        //todo: hack: fixme:
        if($account->id == 1153)
            echo $this->render('_rate-full', ['account' => $account]);
        else
            echo $this->render('_rate', ['account' => $account]);
        ?>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span
                                    class="caption-subject font-green-sharp"><strong>ENGAGEMENT RATE (ACTIVITY)</strong></span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="chart_er" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="caption-subject font-green-sharp"><strong>ИСХОДЯЩИХ ПОДПИСОК</strong></span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="chart_following" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span
                                    class="caption-subject font-green-sharp"><strong>ЛАЙКОВ (ср.)</strong></span></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="chart_like" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span
                                    class="caption-subject font-green-sharp"><strong>КОММЕНТАРИЕВ (ср.)</strong></span></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="chart_comment" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <?php
        if (file_exists( \Yii::getAlias('@webroot') .  '/files/' . $account->username . '.zip')){
            echo $this->render('_audience', ['account' => $account, 'topFollower' => $topFollower]);
        }
        ?>
    </div>
