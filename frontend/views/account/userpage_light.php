<?php

/* @var $this yii\web\View */
/* @var $account common\models\StatsAccounts */

use yii\helpers\Html;
use frontend\utility\HtmlBlocks;
use frontend\assets\UserPageAsset;
use common\utility\Number;


$this->title = 'About ' . $account->username;
$this->params['breadcrumbs'][] = $this->title;

UserPageAsset::register($this);

?>
    <div class="account-user-info container-fluid">
        <div class="row row-flex well" style="margin-right: 0px; margin-left: 0px;">
            <div class="col-lg-6 col-md-4">
                <div class="row row-flex">
                    <div class="col-md-6 text-center">
                        <figure>
                            <?= Html::img($account->profilePicUrl, ['alt' => $account->fullname, 'class' => 'img-circle img-responsiv']) ?>
                        </figure>
                    </div>
                    <div class="col-md-6">
                        <h2><?= Html::encode($account->fullname) ?></h2>
                        <p><?= HtmlBlocks::InstUrl($account->username, null, ['target' => '_blank']) ?> </p>
                        <p><strong>Биография: </strong> <?= Html::encode($account->biography) ?> </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-8">
                <div class="row row-flex">
                    <div class="col-md-12 hidden-xs text-center" style="height: 35px;">
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong>
                                <?= Html::encode(Number::bd_nice_number($account->followers_count)) ?>
                            </strong>
                        </h3>
                        <p>
                            <small><?= \Yii::t('app', 'Followers') ?></small>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong> <?= Html::encode($account->media_count) ?> </strong></h3>
                        <p>
                            <small><?= \Yii::t('app', 'Media count') ?></small>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong> <?= Html::encode(Number::bd_nice_number($account->like_avg)) ?> </strong></h3>
                        <p>
                            <small><?= \Yii::t('app','Likes (avg)') ?></small>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <h3><strong> <?= Html::encode(Number::bd_nice_number($account->comment_avg)) ?> </strong></h3>
                        <p>
                            <small>Комментариев (ср.)</small>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
