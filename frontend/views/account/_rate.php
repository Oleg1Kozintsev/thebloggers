<?php

/* @var $this yii\web\View */
/* @var $account common\models\StatsAccounts */

use frontend\utility\HtmlBlocks;
?>
<div class="row">
    <div class="col-md-4">
        <div class="dashboard-stat2 bordered dashboard-stat2-short">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup"><?= $account->er ?>%</span>
                    </h3>
                    <small>Engagement Rate</small>
                </div>
                <div class="icon">
                    <i class="fa fa-star-o fa-3x" aria-hidden="true" style="font-size: 20px;"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: <?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%;" class="progress-bar progress-bar-success red-haze"></span>
                </div>
                <div class="status">
                    <div class="status-title">За период</div>
                    <div class="status-number"><span class="change-green"><?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="dashboard-stat2 bordered dashboard-stat2-short">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup"><?= $account->lr ?>%</span>
                    </h3>
                    <small>Love Rate</small>
                </div>
                <div class="icon">
                    <i class="fa fa-heart-o fa-3x" aria-hidden="true" style="font-size: 20px;"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: <?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%;" class="progress-bar progress-bar-success red-haze"></span>
                </div>
                <div class="status">
                    <div class="status-title">За период</div>
                    <div class="status-number"><span class="change-green"><?= HtmlBlocks::getSign(round($account->diffEr, 2)) ?>%</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="dashboard-stat2 bordered dashboard-stat2-short">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup"><?= $account->tr ?>%</span>
                    </h3>
                    <small>Talk Rate</small>
                </div>
                <div class="icon">
                    <i class="fa fa-comments-o fa-3x" aria-hidden="true" style="font-size: 20px;"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success green-sharp"></span>
                </div>
                <div class="status">
                    <div class="status-title">За период</div>
                    <div class="status-number"><span class="change-green">0.00%</span></div>
                </div>
            </div>
        </div>
    </div>
</div>

