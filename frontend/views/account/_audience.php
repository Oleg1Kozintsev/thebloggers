<?php
/* @var $this yii\web\View */
/* @var $account common\models\cache\CacheStatsAccounts */
/* @var $topFollower yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\utility\Number;
use common\utility\Datetime;
use rmrevin\yii\fontawesome\FA;
use frontend\utility\HtmlBlocks;


?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="caption-subject font-green-sharp"><strong>АУДИТОРИЯ</strong></span>
        </h3>
    </div>
    <div class="panel-body">
        <?= Html::a('Экспорт в CSV', Url::to(['files/' . $account->username . '.zip']), ['class' => 'btn btn-default']); ?>
        Дата создания файла:
        <?= Datetime::getDataTime(filemtime (\Yii::getAlias('@webroot') .  '/files/' . $account->username . '.zip')); ?>
    </div>
    <div class="panel-footer text-center">
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="caption-subject font-green-sharp"><strong>ТОП ПОДПИСЧИКОВ</strong></span></h3>
            </div>
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $topFollower,
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
                    'summary' => '',
                    'tableOptions' => [
                        'class' => 'table table-bordered'
                    ],
                    'columns' => [
                        array(
                            'label' => '',
                            'encodeLabel' => false,
                            'format' => 'raw',
                            'value' => function ($model, $key, $index) {
                                $number = $index + 1;

                                $url = Yii::$app->urlManager->createAbsoluteUrl('account?id=' . $model->follower_id);
                                $str =  '<div class="w-a"><a href="%s"><img class="w-a" src="%s" alt="">'
                                    .'</div><div class="w-p"><div class="tbl-post">'. $number .'. %s </div></a>'
                                    .'<div class="tbl-sign">%s</div></div>';
                                return sprintf($str, $url, $model->profilePicUrl, $model->username, $model->fullname);
                            },
                        ),
                        [
                            'label' => '<div class="tbl-header">' . \Yii::t('app','Followers') . '&nbsp;' . FA::icon('sort') .'</div>',
                            'attribute' => 'followers_count',
                            'contentOptions' =>['class' => 'table_class'],
                            'encodeLabel' => false,
                            'format' => 'raw',
                            'value' => function ($model) {
                                $count = Number::a_number_format($model->followers_count, 0, '.'," ",3);
                                return HtmlBlocks::IndexCellNum(0, $count);
                            },
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span
                            class="caption-subject font-green-sharp"><strong>ДОСЯГАЕМОСТЬ</strong></span></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div id="pie_reach" style="width: 100%; height: 500px;"></div>
                    <div class="chart-sub"><p style="text-align: center">
                            Три категории подписчиков разделёных по вероятности просмотра постов данного аккаунта.
                        </p>
                        <p style="text-align: center">
                            Высока - подписчики имеющие не более 300 исходящих подписок, средняя - от 301 до 1000, низкая - свыше 1000 исходящих подписок.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span
                            class="caption-subject font-green-sharp"><strong>АУДИТОРИЯ</strong></span></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div id="pie_audience" style="width: 100%; height: 500px;"></div>
                    <div class="chart-sub"><p style="text-align: center">
                            Другое - различные магазины, рестораны, торговые центра и т.д.
                        </p>
                        <p style="text-align: center">
                            Массфоловеры - те, у кого более 1000 подписок.
                        </p>
                        <p style="text-align: center">
                            Пустые аккаунты - у которых не более 15 постов.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span
                            class="caption-subject font-green-sharp"><strong>ПОПУЛЯРНОСТЬ ПОДПИСЧИКОВ</strong></span></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div id="chart_popularityOfFollowers" style="width: 100%; height: 500px;"></div>
                    <div class="chart-sub">
                        <p style="text-align: center">
                            Распределение вашей аудитории по количеству подписчиков
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-g">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span
                            class="caption-subject font-green-sharp"><strong>ПОДПИСКИ ПОДПИСЧИКОВ</strong></span></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div id="chart_popularityOfFollowing" style="width: 100%; height: 500px;"></div>
                    <div class="chart-sub">
                        <p style="text-align: center">
                            Распределение вашей аудитории по максимальному количеству их исходящих подписок
                        </p>
                        <p style="text-align: center">
                            8000 - аудитория, имеющая от 7500 исходящих подписок
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
