<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InstSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
        'id' => 'search-form',
        'action' => ['country'],
        'method' => 'get',
]); ?>
<div class="input-group stylish-input-group">

    <?= $form->field($model, 'search_str', ['template' => "{label}\n{input}"])->textInput(['class' => 'form-control', 'placeholder' => \Yii::t('app','Search')])->label(false) ?>
    <span class="input-group-addon">
        <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span>') ?>
    </span>
</div>

<?php ActiveForm::end(); ?>


