<?php

use yii\grid\GridView;
use common\utility\Number;
use frontend\utility\HtmlBlocks;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\InstSearch */
/* @var $country string */

$this->title = 'The Bloggers';
//$country2 = $country;
//if ($country == 'russia') $country = 'Россия';
//
//$this->params['breadcrumbs'][] =  [
//    'label' => $country,
//    'url'   => '/country/' . $country2
//];

?>
<div class="container-fluid">

    <div style="padding-bottom: 5px;">Страна: Россия</div>

    <div class="row">
        <div class="col-xs-12">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>
                        <?= \Yii::t('app','Saved') . '! ' ?>
                    </h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-xs-12">
            <?
            $icon = '<i class="fa fa-sort" aria-hidden="true"></i>';

            echo GridView::widget([
                 'dataProvider' => $dataProvider,
                 'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
                 'summary' => '',
                 'tableOptions' => [
                     'class' => 'table table-bordered'
                  ],
                    'columns' => [
                        array(
                            'label' => $this->render('_search', array('model' => $searchModel)),
                            'contentOptions' => array('style'=>'width: 28%;'),
                            'encodeLabel' => false,
                            'format' => 'raw',
                            'value' => function ($model, $key, $index) {
                                $number = $index + 1;
                                $page = Yii::$app->request->get('page');
                                if (isset($page) && is_numeric($page)){
                                    $number = $index + 1;
                                    for ($i = 1; $i < $page; $i++){
                                        $number += 50;
                                    }
                                }
                                $lock_icon = $model->req_auth == 1 ? FA::icon('lock') : '';

                                $url = Yii::$app->urlManager->createAbsoluteUrl('account?id=' . $model->id);
                                $str =  '<div class="w-a"><a href="%s"><img class="w-a" src="%s" alt="">'
                                        .'</div><div class="w-p"><div class="tbl-post">'. $number .'. %s '. $lock_icon . '</div></a>'
                                        .'<div class="tbl-sign">%s</div></div>';
                                return sprintf($str, $url, $model->profilePicUrl, $model->username, $model->fullname);
                            },
                        ),
                        [
                                'label' => '<div class="tbl-header">' . \Yii::t('app','Followers') . '&nbsp;' . $icon .'</div>',
                                'attribute' => 'followers_count',
                                'contentOptions' =>['class' => 'table_class','style'=>'text-align: center; width: 12%;'],
                                'encodeLabel' => false,
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $count = Number::a_number_format($model->followers_count, 0, '.'," ",3);
                                    $diff = $model->diffFollowers;
                                    return HtmlBlocks::IndexCellNum($diff, $count);
                                },
                        ],
                        [
                            'label' => '<div class="tbl-header">' . \Yii::t('app','Media count') . '&nbsp;' . $icon . '</div>',
                            'attribute' => 'media_count',
                            'contentOptions' =>['class' => 'table_class','style'=>'text-align: center; width: 12%;'],
                            'encodeLabel' => false,
                            'format' => 'raw',
                            'value' => function ($model) {
                                $count = Number::a_number_format($model->media_count, 0, '.'," ",3);
                                $diff = $model->diffMedia;
                                return HtmlBlocks::IndexCellNum($diff, $count);
                            },
                        ],
                        [
                            'label' => '<div class="tbl-header">' . \Yii::t('app','Likes (avg)') . '&nbsp;' . $icon . '</div>',
                            'attribute' => 'like_avg',
                            'encodeLabel' => false,
                            'contentOptions' =>['class' => 'table_class','style'=>'text-align: center; width: 12%;'],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $diff = $model->diffLikeAvg;
                                $count = Number::a_number_format($model->like_avg, 0, '.'," ",3);
                                return HtmlBlocks::IndexCellNum($diff, $count);
                            },
                        ],
                        [
                            'label' => '<div class="tbl-header">' . \Yii::t('app','Сomments') . '&nbsp;' . $icon . '</div>',
                            'attribute' => 'comment_avg',
                            'encodeLabel' => false,
                            'contentOptions' =>['class' => 'table_class','style'=>'text-align: center; width: 12%;'],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $diff = $model->diffCommentAvg;
                                $count = Number::a_number_format($model->comment_avg, 0, '.'," ",3);
                                return HtmlBlocks::IndexCellNum($diff, $count);
                            },
                        ],
                        [
                            'label' => '<div class="tbl-header">' . \Yii::t('app','ER') . ', %&nbsp;' . $icon . '</div>',
                            'attribute' => 'er',
                            'encodeLabel' => false,
                            'contentOptions' =>['class' => 'table_class','style'=>'text-align: center; width: 12%;'],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $diff = round($model->diffEr, 2);
                                return HtmlBlocks::IndexCellNum($diff, $model->er);
                            },
                        ],
                        [
                            'label' => '<div class="tbl-header">' . \Yii::t('app','Rating') .  '&nbsp;' . $icon . '</div>',
                            'attribute' => 'rating',
                            'encodeLabel' => false,
                            'contentOptions' =>['class' => 'table_class','style'=>'text-align: center; width: 12%;'],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $diff = $model->diffRating;
                                $count = Number::a_number_format($model->rating, 0, '.'," ",3);
                                return HtmlBlocks::IndexCellNum($diff, $count);
                            },
                        ]
                    ],
                 'pager' => [
                    'class' => \kop\y2sp\ScrollPager::className(),
                    'container' => '.grid-view tbody',
                    'item' => 'tr',
                    'paginationSelector' => '.grid-view .pagination',
                    'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
                    'triggerText' => \Yii::t('app','Load more items'),
                    'noneLeftTemplate' => '<div class="ias-noneleft" style="text-align: center; font-size: 12px;">{text}</div>',
                    'noneLeftText' => \Yii::t('app','You reached the end'),
                 ],
            ]);
            ?>
        </div>
    </div>
</div>