<?php
namespace frontend\controllers;

use yii\rest\Controller;
use common\models\StatsLikesLastPost;

class LastPostController extends Controller
{
    public $modelClass = 'common\models\StatsLikesLastPost';

    public function actionIndex($id)
    {
        StatsLikesLastPost::findForAccount($id);
    }

}