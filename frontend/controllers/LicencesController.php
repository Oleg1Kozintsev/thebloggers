<?php
namespace frontend\controllers;

use yii\rest\Controller;
use common\models\Licences;


class LicencesController extends Controller
{
    public $modelClass = 'common\models\Licences';

    /**
     * @param $id
     */
    public function actionIndex($id)
    {
        Licences::findHardwareId($id);
    }
}