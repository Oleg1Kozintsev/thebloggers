<?php
namespace frontend\controllers;

use common\models\cache\CacheStatsAccounts;
use common\models\cache\CacheTopFollowers;
use common\models\LocationForm;
use common\models\StatsAccounts;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

class AccountController extends Controller
{
    /**
     * example http://thebloggers/account?id=123
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id)
    {
        if (($account = CacheStatsAccounts::findUsername($id)) !== null) {
            $locationForm = new LocationForm();
            $locationForm->getUser($id);
            if (\Yii::$app->request->isAjax && $locationForm->load(Yii::$app->request->post()))
            {
                if (!Yii::$app->user->isGuest){
                    $locationForm->save();
                    return 'Request success by location form';    
                } else {
                     return 'Request success by location form but access denied'; 
                }
            } else {
                $locationForm->country = $account->country;
                $locationForm->city = $account->city;
            }
            $topLike = CacheStatsAccounts::getTopLikePhoto($account->id);
            $topComment = CacheStatsAccounts::getTopCommentPhoto($account->id);
            $topFollower = new ActiveDataProvider([
                'query' => CacheTopFollowers::getTopFollowers($id),
                'pagination' => [
                    'pageSize' => 30,
                ],
            ]);
            return $this->render('userpage', [
                'account' => $account,
                'locationForm' => $locationForm,
                'topFollower' => $topFollower,
                'topLike' => $topLike,
                'topComment' => $topComment
            ]);
        } else {
            if (($account = StatsAccounts::findUsername($id)) !== null){
                return $this->render('userpage_light', [
                    'account' => $account
                ]);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
}