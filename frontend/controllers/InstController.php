<?php
/*
 * Контроллер вернёт списко всех акаунтов текущего пользователя, авторизация с помощью сессии
 */
namespace frontend\controllers;

use common\models\Inst;
use yii\rest\Controller;

class InstController extends Controller
{
    public $modelClass = 'common\models\Inst';

    public function actionIndex()
    {
        Inst::findInstByCurrentUser();
    }

}