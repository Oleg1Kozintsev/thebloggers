<?php

namespace frontend\controllers;

use common\models\Audience;

use yii\web\Controller;


class AudienceController extends Controller
{
    public $modelClass = 'common\models\Audience';

    /** http://thebloggers/audience?id=1153
     * @param $id
     */
    public function actionIndex($id)
    {
        Audience::getDataJson($id);
    }
}