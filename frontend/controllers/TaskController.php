<?php
namespace frontend\controllers;

use yii\rest\Controller;
use common\models\Task;


class TaskController extends Controller
{
    public $modelClass = 'common\models\Task';

    /**
     * @param $id
     */
    public function actionIndex($id)
    {
        Task::findAllTaskByInstId($id);
    }
}