<?php

namespace frontend\controllers;

use yii\rest\Controller;
use common\models\StatsTimeline;

class ChartController extends Controller
{
    public $modelClass = 'common\models\StatsTimeline';

    public function actionIndex($id, $start_data = null, $end_data = null)
    {
        if ($start_data == null && $end_data == null){
            StatsTimeline::findStatisticForAccount($id);
        } elseif ($start_data != null && $end_data == null){
            StatsTimeline::findStatisticForAccount($id, $start_data);
        } else {
            StatsTimeline::findStatisticForAccount($id, $start_data, $end_data);
        }
    }
}