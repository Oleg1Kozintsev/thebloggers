<?php

namespace frontend\controllers;

use yii\rest\Controller;
use common\models\StatPost;


class StatPostController extends Controller
{
    public $modelClass = 'common\models\StatPost';

    public function actionIndex($id, $start_data = null, $end_data = null)
    {
        if ($start_data == null && $end_data == null){
            StatPost::findStatisticForAccount($id);
        } elseif ($start_data != null && $end_data == null){
            StatPost::findStatisticForAccount($id, $start_data);
        } else {
            StatPost::findStatisticForAccount($id, $start_data, $end_data);
        }
    }
}