<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;

/**
 * This is the model class for table "{{%statistic_timeline_account}}".
 *
 * @property integer $id
 * @property integer $instagram_id
 * @property integer $created_at
 * @property integer $followers_count
 * @property integer $followings_count
 * @property integer $media_count
 * @property integer $like_avg
 * @property integer $comment_avg
 * @property integer $rating
 * @property integer $er
 */
class StatsTimeline extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stats_timeline}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instagram_id'], 'required'],
            [['instagram_id', 'created_at', 'followers_count', 'followings_count', 'media_count'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instagram_id' => 'Instagram ID',
            'created_at' => 'Created At',
            'followers_count' => 'Followers Count',
            'followings_count' => 'Followings Count',
            'media_count' => 'Media Count',
            'like_avg' => 'Like avg',
            'comment_avg' => 'Comment avg'
        ];
    }

    /**
     * @inheritdoc
     * @return StatsTimelineQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatsTimelineQuery(get_called_class());
    }

    /**
     * Create response JSON by REST API
     * @param $id
     * @param int $start_data
     * @param int $end_data
     * @return array|StatsTimeline[]
     */
    public static function findStatisticForAccount($id, $start_data = 0, $end_data = 0)
    {
        if ($start_data == 0 && $end_data == 0){
            $data = static::find()->where(['instagram_id' => $id])->all();
        } elseif ($end_data == 0 && $start_data != 0) {
            $data = static::find()->where(['instagram_id' => $id])
                ->andWhere('created_at >= :created_at',[':created_at' => $start_data])->all();
        } else {
            $data = static::find()->where(['instagram_id' => $id])
                ->andWhere(['between', 'created_at', $start_data, $end_data])->all();
        }
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;
        return $data;
    }
}
