<?php

namespace common\models;

use yii\base\Model;
use common\models\StatsAccounts;


class LocationForm extends Model
{
    public $country;
    public $city;

    /**
     * @var \common\models\StatsAccounts
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'city'], 'default', 'value' => null],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'country' => \Yii::t('app','Country'),
            'city' => \Yii::t('app','City'),
        ];
    }

    /**
     * Finds user by [[username]]
     *
     * @param $id
     * @return User|null
     */
    public function getUser($id)
    {
        if ($this->_user === null) {

            $this->_user = StatsAccounts::findUsername($id);
        }
        return $this->_user;
    }

    /**
     * @return bool
     */
    public function save(){
        if ($this->_user !== null){
            $user = $this->_user;
            $user->country = $this->country;
            $user->city = $this->city;

            return $user->save(false);
        }
        return false;
    }
}