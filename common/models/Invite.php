<?php

namespace common\models;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%invite}}".
 *
 * @property string $invite_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $status
 */
class Invite extends ActiveRecord
{
    const STATUS_FOR_ALL = 10;
    const STATUS_FOR_USER = 20;
    const STATUS_NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invite}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invite_id'], 'required'],
            [['user_id', 'created_at', 'status'], 'integer'],
            [['invite_id'], 'string', 'max' => 50],
            ['status', 'default', 'value' => self::STATUS_FOR_ALL],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invite_id' => 'Invite ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @return InviteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InviteQuery(get_called_class());
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_FOR_ALL => 'Для всех пользователей',
            self::STATUS_FOR_USER => 'Персональный',
            self::STATUS_NOT_ACTIVE => 'Не действительный',
        ];
    }

    public static function findIdentity($invite_id, $user_id = null)
    {
        if ($user_id == null){
            return static::findOne(['invite_id' => $invite_id, 'status' => self::STATUS_FOR_ALL]);
        } else {
            return static::findOne(['invite_id' => $invite_id, 'user_id' => $user_id, 'status' => self::STATUS_FOR_USER]);
        }
    }

    public static function validateInvite($invite_id, $user_id = null){
        if ($user_id == null){
            return static::findOne(['invite_id' => $invite_id, 'status' => self::STATUS_FOR_ALL]) != null;
        } else {
            return static::findOne(['invite_id' => $invite_id, 'user_id' => $user_id, 'status' => self::STATUS_FOR_USER]) != null;
        }
    }

    public static function setStatusNonActive($invite_id){
        $row = static::findOne(['invite_id' => $invite_id]);
        if ($row != null){
            $row->status = self::STATUS_NOT_ACTIVE;
            $row->save();
        }
    }
}
