<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;


/**
 * This is the model class for table "instagram".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $foto
 * @property string $fullname
 *
 * @property integer $user_id
 * @property integer $idproxy
 */
class Inst extends ActiveRecord
{
    public static function tableName()
    {
        return 'instagram';
    }

    /**
     * @param bool $needResponse
     * @return array|ActiveRecord[]
     */
    public static function findInstByCurrentUser($needResponse = true)
    {
        $user_id = Yii::$app->user->id;
        $data = static::find()->where(['user_id' => $user_id])->all();
        if ($needResponse){
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = $data;
        }
        return $data;
    }

    /**
     * @return array - выводить только эти поля
     */
    public function fields()
    {
        return ['id', 'username', 'fullname', 'foto', 'user_id'];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'string', 'max' => 255],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     * Пример получение связи один - ко - многим
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['id_instagram' => 'id']);
    }
}