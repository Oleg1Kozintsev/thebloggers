<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;

/**
 * This is the model class for table "audience".
 *
 * @property integer $id
 * @property integer $accounts_id
 * @property string $type
 * @property string $name
 * @property integer $value
 * @property string $name_ru
 */
class Audience extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audience';
    }

    public static function getDataJson($id) {
        $data = static::find()
            ->select(['type', 'name', 'value', 'name_ru'])
            ->where(['accounts_id' => $id])
            ->all();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;
        return $data;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accounts_id', 'type', 'name', 'value'], 'required'],
            [['accounts_id', 'value'], 'integer'],
            [['type', 'name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'accounts_id' => 'Accounts ID',
            'type' => 'Type',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }

    /**
     * @inheritdoc
     * @return AudienceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AudienceQuery(get_called_class());
    }
}
