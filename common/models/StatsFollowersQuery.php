<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[StatsFollowers]].
 *
 * @see StatsFollowers
 */
class StatsFollowersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StatsFollowers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatsFollowers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
