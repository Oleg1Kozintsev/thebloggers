<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;

/**
 * This is the model class for table "{{%licences}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $hardware_id
 * @property integer $block
 * @property integer $create_at
 * @property integer $limit
 */
class Licences extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%licences}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block', 'create_at', 'limit'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['hardware_id'], 'string', 'max' => 65],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'hardware_id' => 'Hardware ID',
            'block' => 'Block',
            'create_at' => 'Create At',
        ];
    }

    /**
     * @return array поля которые будет возвращаться
     */
    public function fields()
    {
        return [
            // название поля совпадает с названием атрибута
            'hardware_id',
            'create_at'
        ];
    }

    /**
     * @param $hardware_id
     * @return string
     */
    public static function findHardwareId($hardware_id)
    {
        $data = static::find()->where(['hardware_id' => $hardware_id, 'block' => 0])->all();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;
        return $data;
    }
}
