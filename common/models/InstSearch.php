<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\cache\CacheNewAccounts;
use common\models\cache\CacheStatsAccounts;


/**
 * InstSearch represents the model behind the search form about `common\models\StatsAccounts`.
 */
class InstSearch extends CacheStatsAccounts
{

    public $search_str;
    public $diffFollowers;
    public $diffMedia;
    public $diffLikeAvg;
    public $diffCommentAvg;
    public $diffEr;
    public $diffRating;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'fullname', 'diffFollowers', 'diffMedia'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CacheStatsAccounts::find();

        $this->load($params);

        if (!Yii::$app->user->isGuest && isset($params['InstSearch']) && isset($params['InstSearch']['search_str'])){
            $searchVal = $params['InstSearch']['search_str'];
            if (isset($searchVal)){
                $result = $query->andFilterWhere(['like', 'username', $searchVal])
                    ->all();
                if (count($result) == 0){
                    // это значить данных о пользователе нет в базе данных, здесь его нужно добавить
                    $newAccount = new CacheNewAccounts();
                    $newAccount->username = $searchVal;
                    $newAccount->is_sync = 0;
                    $newAccount->is_error = 0;
                    $newAccount->save();
                    Yii::$app->session->setFlash('success', \Yii::t('app','Username') . ': ' . $searchVal );
                }
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
                ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (isset($params['InstSearch']) && isset($params['InstSearch']['search_str'])){
            $this->search_str = $params['InstSearch']['search_str'];
            $query->orFilterWhere(['like', 'username', $this->search_str])
                ->orFilterWhere(['like', 'fullname', $this->search_str]);
        }


        $dataProvider->setSort([
            'attributes' => [
                'username' => [
                    'asc' => ['username' => SORT_ASC],
                    'desc' => ['username' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'followers_count' => [
                    'asc' => ['followers_count' => SORT_ASC],
                    'desc' => ['followers_count' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'media_count' => [
                    'asc' => ['media_count' => SORT_ASC],
                    'desc' => ['media_count' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'like_avg' => [
                    'asc' => ['like_avg' => SORT_ASC],
                    'desc' => ['like_avg' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'comment_avg' => [
                    'asc' => ['comment_avg' => SORT_ASC],
                    'desc' => ['comment_avg' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'er' => [
                    'asc' => ['er' => SORT_ASC],
                    'desc' => ['er' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'rating' => [
                    'asc' => ['rating' => SORT_ASC],
                    'desc' => ['rating' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
            ],
            'defaultOrder' => [
                'rating' => SORT_DESC
            ]
        ]);

        return $dataProvider;
    }
}
