<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[StatsAccounts]].
 *
 * @see StatsAccounts
 */
class StatsAccountsQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StatsAccounts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatsAccounts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
