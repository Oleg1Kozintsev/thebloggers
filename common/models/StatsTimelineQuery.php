<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[StatsTimeline]].
 *
 * @see StatsTimeline
 */
class StatsTimelineQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StatsTimeline[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatsTimeline|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
