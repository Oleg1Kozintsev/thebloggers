<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%statistics_likers}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $media_id
 * @property string $username
 * @property integer $created_at
 */
class StatisticsLikers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%statistics_likers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'media_id', 'username'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['media_id'], 'string', 'max' => 25],
            [['username'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'media_id' => 'Media ID',
            'username' => 'Username',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     * @return StatisticsLikersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatisticsLikersQuery(get_called_class());
    }
}
