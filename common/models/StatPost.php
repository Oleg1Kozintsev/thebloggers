<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;

/**
 * This is the model class for table "{{%stat_post}}".
 *
 * @property integer $id
 * @property integer $instagram_id
 * @property string $url
 * @property string $caption
 * @property string $code
 * @property integer $taken_at
 * @property integer $pk
 * @property integer $like_count
 * @property integer $comment_count
 *
 * @property StatsAccounts $instagram
 */
class StatPost extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stat_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instagram_id'], 'required'],
            [['instagram_id', 'taken_at', 'pk', 'like_count', 'comment_count'], 'integer'],
            [['caption'], 'string'],
            [['url'], 'string', 'max' => 200],
            [['code'], 'string', 'max' => 45],
            [['pk'], 'unique'],
            [['instagram_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatsAccounts::className(), 'targetAttribute' => ['instagram_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instagram_id' => 'Instagram ID',
            'url' => 'Url',
            'caption' => 'Caption',
            'code' => 'Code',
            'taken_at' => 'Taken At',
            'pk' => 'Pk',
            'like_count' => 'Like Count',
            'comment_count' => 'Comment Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstagram()
    {
        return $this->hasOne(StatsAccounts::className(), ['id' => 'instagram_id']);
    }

    /**
     * @inheritdoc
     * @return StatPostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatPostQuery(get_called_class());
    }

    /**
     * Create response JSON by REST API
     * @param $id
     * @param int $start_data
     * @param int $end_data
     * @return array|StatsTimeline[]
     */
    public static function findStatisticForAccount($id, $start_data = 0, $end_data = 0)
    {
        if ($start_data == 0 && $end_data == 0){
            $data = static::find()->where(['instagram_id' => $id])
                ->select(['like_count', 'comment_count', 'taken_at'])
                ->orderBy('taken_at DESC')->all();
        } elseif ($end_data == 0 && $start_data != 0) {
            $data = static::find()->where(['instagram_id' => $id])
                ->andWhere('taken_at >= :created_at',[':taken_at' => $start_data])
                ->select(['like_count', 'comment_count', 'taken_at'])
                ->orderBy('taken_at DESC')->all();
        } else {
            $data = static::find()->where(['instagram_id' => $id])
                ->andWhere(['between', 'taken_at', $start_data, $end_data])
                ->select(['like_count', 'comment_count', 'taken_at'])
                ->orderBy('taken_at DESC')->all();
        }
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;
        return $data;
    }
}
