<?php
namespace common\models;

use yii\base\Model;
use app\models\Photo;


class FilePhoto extends Model
{
    public $id;
    public $fileName;
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg']
        ];
        
    }
    // todo: сгенерировать случайно имя, сохранить это имя в переменную, для записи в базу данных
    public function upload()
    {
        if ($this->validate()) {
            $this->fileName = uniqid().'.' . $this->imageFile->extension;
            $this->imageFile->saveAs('uploads/' . $this->fileName);
            $photo = new Photo();
            $photo->photo = $this->fileName;
            $photo->save();
            $this->id = $photo->id;
            return true;
        } else {
            return false;
        }
    }

}