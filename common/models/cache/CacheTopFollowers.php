<?php

namespace common\models\cache;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "cache_top_followers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $follower_id
 * @property string $username
 * @property integer $followers_count
 * @property string $fullname
 * @property string  $profilePicUrl
 */
class CacheTopFollowers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cache_top_followers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'follower_id', 'username'], 'required'],
            [['user_id', 'follower_id', 'followers_count'], 'integer'],
            [['username'], 'string', 'max' => 45],
            [['fullname'], 'string', 'max' => 255],
            [['profilePicUrl'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'follower_id' => 'Follower ID',
            'username' => 'Username',
            'followers_count' => 'Followers Count',
            'fullname' => 'Fullname',
        ];
    }

    /**
     * получение топ 30 подписчиков
     * @param $id
     * @return \yii\db\ActiveQuery
     */
    public static function getTopFollowers($id){
        return static::find()
            ->where(['user_id' => $id])
            ->orderBy('followers_count DESC')
            ->limit(30);
    }

    /**
     * @param $id
     * @return array|CacheTopFollowers[]
     */
    public static function getTopFollowersArray($id){
        return static::find()
            ->where(['user_id' => $id])
            ->orderBy('followers_count DESC')
            ->limit(30)
            ->all();
    }

    /**
     * @inheritdoc
     * @return CacheTopFollowersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CacheTopFollowersQuery(get_called_class());
    }
}
