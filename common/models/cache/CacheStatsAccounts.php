<?php

namespace common\models\cache;

use common\models\StatPost;
use common\models\StatsTimeline;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cache_stats_accounts".
 *
 * @property integer $id
 * @property string  $username
 * @property integer $followers_count
 * @property integer $followings_count
 * @property integer $media_count
 * @property string  $fullname
 * @property string  $gender
 * @property string  $email
 * @property string  $biography
 * @property string  $birthday
 * @property string  $latitude
 * @property string  $longitude
 * @property string  $publicphonenumber
 * @property string  $socialcontext
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $profilePicUrl
 * @property integer $pk
 * @property string  $top_photo
 * @property integer $is_sync
 * @property integer $is_error
 * @property integer $is_do
 * @property integer $is_show
 * @property integer $like_avg
 * @property integer $comment_avg
 * @property integer $rating
 * @property float $er
 * @property float $er_followers
 * @property float $lr
 * @property float $lr_followers
 * @property float $tr
 * @property float $tr_followers
 * @property integer $last_media_pk
 * @property string $last_media_url
 * @property string $last_media_caption
 * @property integer $last_media_timestamp
 * @property string $last_media_code
 * @property string location
 * @property string country
 * @property string city
 * @property integer req_auth
 */
class CacheStatsAccounts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cache_stats_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['followers_count', 'followings_count', 'created_at', 'updated_at', 'pk'], 'integer'],
            [['username', 'gender', 'email', 'birthday', 'latitude', 'longitude', 'publicphonenumber'], 'string', 'max' => 45],
            [['fullname', 'biography', 'socialcontext', 'profilePicUrl'], 'string', 'max' => 255],
            [['username'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'followers_count' => 'Followers Count',
            'followings_count' => 'Followings Count',
            'fullname' => 'Fullname',
            'gender' => 'Gender',
            'email' => 'Email',
            'biography' => 'Biography',
            'birthday' => 'Birthday',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'publicphonenumber' => 'Publicphonenumber',
            'socialcontext' => 'Socialcontext',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'profilePicUrl' => 'Profile Pic Url',
            'pk' => 'Pk',
            'diffFollowers' => 'diffFollowers',
            'diffLikeAvg' => 'diffLikeAvg',
            'diffCommentAvg' => 'diffCommentAvg',
            'diffEr' => 'diffEr',
            'diffRating' => 'diffRating'
        ];
    }

    /**
     * Вычисление разницы в фоловерах, тут должны быть логика по вычислению
     * берём id получаем массив из таблицы StatsTimeline и вычисяем разницу
     * @return int
     */
    public function getDiffFollowers() {
        $model = $this->getTimelineModel('followers_count');
        if (isset($model))
            return $this->followers_count - $model->followers_count;
        return 0;
    }

    /**
     * @return int
     */
    public function getDiffMedia() {
        $model = $this->getTimelineModel('media_count');
        if (isset($model))
            return $this->media_count - $model->media_count;
        return 0;
    }

    /**
     * @return int
     */
    public function getDiffLikeAvg() {
        $model = $this->getTimelineModel('like_avg');
        if (isset($model))
            return $this->like_avg - $model->like_avg;
        return 0;
    }

    /**
     * @return int
     */
    public function getDiffCommentAvg() {
        $model = $this->getTimelineModel('comment_avg');
        if (isset($model))
            return $this->comment_avg - $model->comment_avg;
        return 0;
    }

    /**
     * @return int|mixed
     */
    public function getDiffEr(){
        $model = $this->getTimelineModel('er');
        if (isset($model))
            return $this->er - $model->er;
        return 0;
    }

    /**
     * @return int|mixed
     */
    public function getDiffRating(){
        $model = $this->getTimelineModel('rating');
        if (isset($model))
            return $this->rating - $model->rating;
        return 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimelineStatistic()
    {
        return $this->hasMany(StatsTimeline::className(), ['instagram_id' => 'id']);
    }


    public static function findUsername($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Получаем топ фото из связанной таблице
     * @param $id
     * @return array|StatPost[]
     */
    public static function getTopLikePhoto($id) {
        return StatPost::find()
            ->select(['url', 'caption', 'code', 'taken_at', 'like_count', 'comment_count'])
            ->where(['instagram_id' => $id])
            ->orderBy('like_count DESC')
            ->limit(20)
            ->all();
    }

    /**
     * @param $id
     * @return array|StatPost[]
     */
    public static function getTopCommentPhoto($id) {
        return StatPost::find()
            ->select(['url', 'caption', 'code', 'taken_at', 'like_count', 'comment_count'])
            ->where(['instagram_id' => $id])
            ->orderBy('comment_count DESC')
            ->limit(20)
            ->all();
    }

    /**
     * @param $id
     * @return array|StatPost|null
     */
    public static function getLastPhoto($id) {
        return StatPost::find()
            ->select(['url', 'caption', 'code', 'taken_at', 'like_count', 'comment_count'])
            ->where(['instagram_id' => $id])
            ->orderBy('taken_at DESC')
            ->limit(1)
            ->one();
    }

    /**
     * @param $field
     * @return ActiveRecord|array|null
     */
    private function getTimelineModel($field) {
        if (isset($this->updated_at)){
            $max = StatsTimeline::find() // getting AQ instance
            ->andWhere(['instagram_id' => $this->id]) // also some conditions
            ->andWhere(['<>' ,'created_at', $this->updated_at])
                ->select('id') // we need only one column
                ->max('id');
        } else {
            return null;
        }

        if (empty($max)) return null;

        $model = StatsTimeline::find()
            ->andWhere(['id' => $max])->select($field)->one();

        return $model;
    }

    /**
     * @inheritdoc
     * @return CacheStatsAccountsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CacheStatsAccountsQuery(get_called_class());
    }
}
