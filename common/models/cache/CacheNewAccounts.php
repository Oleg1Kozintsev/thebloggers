<?php

namespace common\models\cache;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%cache_new_accounts}}".
 *
 * @property integer $id
 * @property string $username
 * @property integer $is_sync
 * @property integer $is_error
 */
class CacheNewAccounts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cache_new_accounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['is_sync', 'is_error'], 'integer'],
            [['username'], 'string', 'max' => 45],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'is_sync' => 'Is Sync',
            'is_error' => 'Is Error',
        ];
    }

    /**
     * @inheritdoc
     * @return CacheNewAccountsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CacheNewAccountsQuery(get_called_class());
    }
}
