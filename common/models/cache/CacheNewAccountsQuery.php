<?php

namespace common\models\cache;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CacheNewAccounts]].
 *
 * @see CacheNewAccounts
 */
class CacheNewAccountsQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CacheNewAccounts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CacheNewAccounts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
