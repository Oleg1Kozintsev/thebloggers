<?php

namespace common\models\cache;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CacheTopFollowers]].
 *
 * @see CacheTopFollowers
 */
class CacheTopFollowersQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CacheTopFollowers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CacheTopFollowers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
