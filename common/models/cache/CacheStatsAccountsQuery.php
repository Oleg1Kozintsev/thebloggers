<?php

namespace common\models\cache;

/**
 * This is the ActiveQuery class for [[CacheStatsAccounts]].
 *
 * @see CacheStatsAccounts
 */
class CacheStatsAccountsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CacheStatsAccounts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CacheStatsAccounts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
