<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[StatPost]].
 *
 * @see StatPost
 */
class StatPostQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StatPost[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatPost|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
