<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $idproxy
 * @property string $iporhost
 * @property string $port
 * @property string $login
 * @property string $password
 * @property string $host_type
 *
 * @property Inst $inst
 */
class Proxy extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iporhost', 'port'], 'string', 'max' => 255],
            [['login', 'password'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idproxy' => 'Idproxy',
            'iporhost' => 'Host',
            'port' => 'Port',
            'login' => 'Login',
            'password' => 'Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInst()
    {
        return $this->hasOne(Inst::className(), ['idproxy' => 'idproxy']);
    }

    /**
     * @inheritdoc
     * @return ProxyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProxyQuery(get_called_class());
    }
}
