<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "stats_followers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $follower_id
 * @property integer $created_at
 */
class StatsFollowers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_followers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'follower_id'], 'required'],
            [['user_id', 'follower_id', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'follower_id' => 'Follower ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     * @return StatsFollowersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatsFollowersQuery(get_called_class());
    }
}
