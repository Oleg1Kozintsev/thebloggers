<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "stats_accounts".
 *
 * @property integer $id
 * @property string  $username
 * @property integer $followers_count
 * @property integer $followings_count
 * @property integer $media_count
 * @property string  $fullname
 * @property string  $gender
 * @property string  $email
 * @property string  $biography
 * @property string  $birthday
 * @property string  $latitude
 * @property string  $longitude
 * @property string  $publicphonenumber
 * @property string  $socialcontext
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $profilePicUrl
 * @property integer $pk
 * @property string  $top_photo
 * @property integer $is_sync
 * @property integer $is_error
 * @property integer $is_do
 * @property integer $is_show
 * @property integer $like_avg
 * @property integer $comment_avg
 * @property integer $rating
 * @property float $er
 * @property float $lr
 * @property float $lr_followers
 * @property float $tr
 * @property string location
 * @property string country
 * @property string city
 * @property integer req_auth
 */
class StatsAccounts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['followers_count', 'followings_count', 'created_at', 'updated_at', 'pk'], 'integer'],
            [['username', 'gender', 'email', 'birthday', 'latitude', 'longitude', 'publicphonenumber'], 'string', 'max' => 45],
            [['fullname', 'biography', 'socialcontext', 'profilePicUrl'], 'string', 'max' => 255],
            [['username'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'followers_count' => 'Followers Count',
            'followings_count' => 'Followings Count',
            'fullname' => 'Fullname',
            'gender' => 'Gender',
            'email' => 'Email',
            'biography' => 'Biography',
            'birthday' => 'Birthday',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'publicphonenumber' => 'Publicphonenumber',
            'socialcontext' => 'Socialcontext',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'profilePicUrl' => 'Profile Pic Url',
            'pk' => 'Pk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimelineStatistic()
    {
        return $this->hasMany(StatsTimeline::className(), ['instagram_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return StatsAccountsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatsAccountsQuery(get_called_class());
    }

    public static function findUsername($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
}
