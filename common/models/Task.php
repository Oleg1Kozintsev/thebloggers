<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\web\Response;
use common\utility\Datetime;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $title
 * @property integer $task_time
 * @property integer $start
 * @property integer $end
 * @property string $task
 * @property integer $launched
 * @property integer $completed
 * @property integer $id_instagram
 * @property integer $id_case
 *
 * @property Inst $idInstagram
 * @property TaskCase $idCase
 */
class Task extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id'];
    }
	
	/**
     * @param $id_instagram
     * @return string
     */
    public static function findAllTaskByInstId($id_instagram)
    {
        $data = static::find()->where(['id_instagram' => $id_instagram])->all();
        // todo: тут должна быть провекрка текущего пользователя
        // c помощью getInst получаем модель инстаграмма и сравниваем с id пользователя
        // если id не совпадают ничего не возвращаем
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        foreach ($data as $item)
        {
            $item['task_time'] = Datetime::getData($item['task_time']);
            $item['start']     = Datetime::getData($item['start']);
            $item['end']       = Datetime::getData($item['end']);
        }
        $response->data = $data;
        return $data;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_time', 'start', 'end', 'task', 'id_instagram', 'id_case'], 'required'],
            [['task_time', 'start', 'end'], 'safe'],
            [['task'], 'string'],
            [['launched', 'сompleted', 'id_instagram', 'id_case'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['id_instagram'], 'exist', 'skipOnError' => true, 'targetClass' => Inst::className(), 'targetAttribute' => ['id_instagram' => 'id']],
            //[['id_case'], 'exist', 'skipOnError' => true, 'targetClass' => TaskCase::className(), 'targetAttribute' => ['id_case' => 'id_case']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'task_time' => 'Task Time',
            'start' => 'Start',
            'end' => 'End',
            'task' => 'Task',
            'launched' => 'Launched',
            'сompleted' => 'сompleted',
            'id_instagram' => 'Id Instagram',
            'id_case' => 'Id Case',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery (получение связанного с задачей инстаграмм аккаунта)
     */
    public function getInst()
    {
        return $this->hasOne(Inst::className(), ['id' => 'id_instagram']);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }


    /**
     * @return array поля которые будет возвращаться
     */
    public function fields()
    {
        return [
            // название поля совпадает с названием атрибута
            'title',
            'start',
            'end',
            'launched',
            'сompleted',
            'task_time',
            'task'
        ];
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCase()
    {
        return $this->hasOne(TaskCase::className(), ['id_case' => 'id_case']);
    }
	
	/**
     * @inheritdoc
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

}
