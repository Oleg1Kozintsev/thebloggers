<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;

/**
 * This is the model class for table "{{%stats_likes_last_post}}".
 *
 * @property integer $id
 * @property integer $instagram_id
 * @property integer $created_at
 * @property integer $like_count
 * @property integer $comment_count
 */
class StatsLikesLastPost extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stats_likes_last_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instagram_id'], 'required'],
            [['instagram_id', 'created_at', 'like_count', 'comment_count'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instagram_id' => 'Instagram ID',
            'created_at' => 'Created At',
            'like_count' => 'Like Count',
            'comment_count' => 'Comment Count',
        ];
    }

    public static function findForAccount($id)
    {
        $data = static::find()->where(['instagram_id' => $id])->all();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;
        return $data;
    }
}
