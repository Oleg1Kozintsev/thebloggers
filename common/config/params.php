<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'trmp@inbox.ru',
    'user.passwordResetTokenExpire' => 3600,
];
