<?php
/**
 * Created by PhpStorm.
 * User: Олег
 * Date: 31.05.2017
 * Time: 12:12
 */

namespace common\utility;


class Datetime
{
    /**
     * @param $timestamp
     * @return data
     */
    public static function getData($timestamp)
    {
        $date = new \DateTime();
        // If you must have use time zones
        // $date = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
        return $date->setTimestamp($timestamp)->format('Y-m-d H:i:s');
    }

    /**
     * 10.09.2017 08:45
     * @param $timestamp
     * @return string
     */
    public static function getDataTime($timestamp)
    {
        $date = new \DateTime();
        // If you must have use time zones
        // $date = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
        return $date->setTimestamp($timestamp)->format('d.m.Y H:m');
    }
}