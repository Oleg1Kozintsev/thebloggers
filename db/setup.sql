/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.5.52-MariaDB-cll-lve : Database - supergram
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`supergram` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `supergram`;

/*Table structure for table `instagram` */

DROP TABLE IF EXISTS `instagram`;

CREATE TABLE `instagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `idproxy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_instagram_account_user1_idx` (`user_id`),
  KEY `idproxy` (`idproxy`),
  CONSTRAINT `FK_instagram_proxy` FOREIGN KEY (`idproxy`) REFERENCES `proxy` (`idproxy`),
  CONSTRAINT `FK_instagram_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `invite` */

DROP TABLE IF EXISTS `invite`;

CREATE TABLE `invite` (
  `invite_id` varchar(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`invite_id`),
  KEY `fk_invitee_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `processes` */

DROP TABLE IF EXISTS `processes`;

CREATE TABLE `processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `proxy` */

DROP TABLE IF EXISTS `proxy`;

CREATE TABLE `proxy` (
  `idproxy` int(11) NOT NULL AUTO_INCREMENT,
  `iporhost` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `host_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idproxy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `service_log` */

DROP TABLE IF EXISTS `service_log`;

CREATE TABLE `service_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `stat_post` */

DROP TABLE IF EXISTS `stat_post`;

CREATE TABLE `stat_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instagram_id` bigint(20) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `caption` text,
  `code` varchar(45) DEFAULT NULL,
  `taken_at` int(11) DEFAULT NULL,
  `pk` bigint(20) DEFAULT NULL,
  `like_count` int(11) DEFAULT '0',
  `comment_count` int(11) DEFAULT '0',
  `location` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk` (`pk`),
  KEY `fk_stat_post_stats_accounts1_idx` (`instagram_id`),
  CONSTRAINT `FK_stat_post_stats_accounts` FOREIGN KEY (`instagram_id`) REFERENCES `stats_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65887 DEFAULT CHARSET=utf8;

/*Table structure for table `statistics_likers` */

DROP TABLE IF EXISTS `statistics_likers`;

CREATE TABLE `statistics_likers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistic_like_statistic_instagram_account1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2441 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_accounts` */

DROP TABLE IF EXISTS `stats_accounts`;

CREATE TABLE `stats_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `publicphonenumber` varchar(45) DEFAULT NULL,
  `socialcontext` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  `pk` bigint(20) NOT NULL,
  `top_photo` mediumtext,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  `is_do` tinyint(4) NOT NULL DEFAULT '1',
  `is_show` tinyint(4) NOT NULL DEFAULT '0',
  `like_avg` bigint(20) DEFAULT '0',
  `comment_avg` bigint(20) DEFAULT '0',
  `rating` bigint(20) DEFAULT '0',
  `er` float DEFAULT '0',
  `lr` float DEFAULT '0',
  `tr` float DEFAULT '0',
  `last_media_pk` bigint(20) DEFAULT '0',
  `last_media_url` varchar(255) DEFAULT '0',
  `last_media_caption` varchar(1024) DEFAULT '0',
  `last_media_timestamp` int(11) DEFAULT '0',
  `last_media_code` varchar(25) DEFAULT '0',
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `countryCode` varchar(10) DEFAULT NULL,
  `req_auth` tinyint(4) DEFAULT '0',
  `is_business` tinyint(4) DEFAULT '0',
  `is_404` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `pk` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=9451826 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_followers` */

DROP TABLE IF EXISTS `stats_followers`;

CREATE TABLE `stats_followers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `follower_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistic_followers_statistic_instagram_account1_idx` (`user_id`),
  KEY `fk_statistic_followers_statistic_instagram_account2_idx` (`follower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9567987 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_followings` */

DROP TABLE IF EXISTS `stats_followings`;

CREATE TABLE `stats_followings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `following_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistic_followings_statistic_instagram_account1_idx` (`user_id`),
  KEY `fk_statistic_followings_statistic_instagram_account2_idx` (`following_id`)
) ENGINE=InnoDB AUTO_INCREMENT=956 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_likes_last_post` */

DROP TABLE IF EXISTS `stats_likes_last_post`;

CREATE TABLE `stats_likes_last_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instagram_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `like_count` int(11) DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_time_statistic_statistic_instagram_account1_idx` (`instagram_id`)
) ENGINE=InnoDB AUTO_INCREMENT=244381 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_timeline` */

DROP TABLE IF EXISTS `stats_timeline`;

CREATE TABLE `stats_timeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instagram_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `like_avg` bigint(20) DEFAULT NULL,
  `comment_avg` bigint(20) DEFAULT NULL,
  `rating` bigint(20) DEFAULT NULL,
  `er` float DEFAULT NULL,
  `lr` float DEFAULT NULL,
  `tr` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_time_statistic_statistic_instagram_account1_idx` (`instagram_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27842 DEFAULT CHARSET=utf8;

/*Table structure for table `task` */

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '0',
  `task_time` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `task` varchar(255) NOT NULL,
  `launched` tinyint(4) NOT NULL,
  `сompleted` tinyint(4) NOT NULL,
  `id_instagram` int(11) NOT NULL,
  `id_case` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_task_case1_idx` (`id_case`),
  KEY `fk_task_instagram_account1_idx` (`id_instagram`),
  CONSTRAINT `FK_task_instagram` FOREIGN KEY (`id_instagram`) REFERENCES `instagram` (`id`),
  CONSTRAINT `FK_task_task_case` FOREIGN KEY (`id_case`) REFERENCES `task_case` (`id_case`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*Table structure for table `task_case` */

DROP TABLE IF EXISTS `task_case`;

CREATE TABLE `task_case` (
  `id_case` int(11) NOT NULL AUTO_INCREMENT,
  `casename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_case`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `expired_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `idx-token-user_id` (`user_id`),
  CONSTRAINT `idx-token-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(60) NOT NULL,
  `password_reset_token` varchar(100) DEFAULT NULL,
  `email_confirm_token` varchar(32) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `access_token` varchar(100) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `timezone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
