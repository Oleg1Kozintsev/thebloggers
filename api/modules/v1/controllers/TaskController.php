<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use common\models\Task;


class TaskController extends Controller
{
    public $modelClass = 'common\models\Task';

    public function actionIndex($id)
    {
        Task::findAllTaskByInstId($id);
    }
}