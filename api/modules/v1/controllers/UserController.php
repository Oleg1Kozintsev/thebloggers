<?php
namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use common\models\User;
use yii\filters\auth\HttpBasicAuth;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';

//    public function behaviors()
//    {
//        $behaviors = parent::behaviors();
//        $behaviors['authenticator'] = [
//            'class' => HttpBasicAuth::className(),
//            //'class' => CompositeAuth::className(),
//            // 'authMethods' => [
//            //     HttpBasicAuth::className(),
//            //     HttpBearerAuth::className(),
//            //     QueryParamAuth::className(),
//            // ],
//        ];
//        //Укажем, что создавать, редактировать и удалять листинги могут только авторизованные пользователи (https://habrahabr.ru/post/318242/):
//        $behaviors['access'] = [
//            'class' => AccessControl::className(),
//            'only' => ['create', 'update', 'delete'],
//            'rules' => [
//                [
//                    'actions' => ['create', 'update', 'delete'],
//                    'allow' => true,
//                    'roles' => ['@'],
//                ],
//            ],
//        ];
//
//        return $behaviors;
//    }

    public function actionIndex()
    {
        $model = new User();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    public function checkAccess($action, $model = null, $params = [])
//    {
//        // проверяем может ли пользователь редактировать или удалить запись
//        // выбрасываем исключение ForbiddenHttpException если доступ запрещен
//        if ($action === 'update' || $action === 'delete') {
//            if ($model->user_id !== \Yii::$app->user->id)
//                throw new ForbiddenHttpException(sprintf('You can only %s lease that you\'ve created.', $action));
//        }
//    }
}
